-- MySQL dump 10.13  Distrib 5.7.31, for Linux (x86_64)
--
-- Host: localhost    Database: springbootxxoiz
-- ------------------------------------------------------
-- Server version	5.7.31

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `springbootxxoiz`
--

/*!40000 DROP DATABASE IF EXISTS `springbootxxoiz`*/;

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `springbootxxoiz` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `springbootxxoiz`;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(100) NOT NULL COMMENT '配置参数名称',
  `value` varchar(100) DEFAULT NULL COMMENT '配置参数值',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='配置文件';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
INSERT INTO `config` VALUES (1,'picture1','upload/picture1.jpg'),(2,'picture2','upload/picture2.jpg'),(3,'picture3','upload/picture3.jpg');
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fangyiwuzi`
--

DROP TABLE IF EXISTS `fangyiwuzi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fangyiwuzi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `wuzimingcheng` varchar(200) DEFAULT NULL COMMENT '物资名称',
  `wuzifenlei` varchar(200) DEFAULT NULL COMMENT '物资分类',
  `wuzitupian` varchar(200) DEFAULT NULL COMMENT '物资图片',
  `wuziguige` varchar(200) DEFAULT NULL COMMENT '物资规格',
  `wuzipinpai` varchar(200) DEFAULT NULL COMMENT '物资品牌',
  `wuzishuliang` int(11) DEFAULT NULL COMMENT '物资数量',
  `wuzijieshao` longtext COMMENT '物资介绍',
  `dengjiriqi` datetime DEFAULT NULL COMMENT '登记日期',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=utf8 COMMENT='防疫物资';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fangyiwuzi`
--

LOCK TABLES `fangyiwuzi` WRITE;
/*!40000 ALTER TABLE `fangyiwuzi` DISABLE KEYS */;
INSERT INTO `fangyiwuzi` VALUES (101,'2022-02-11 12:41:51','物资名称1','口罩','upload/fangyiwuzi_wuzitupian1.jpg','物资规格1','物资品牌1',1,'物资介绍1','2022-02-11 20:41:51','管理员账号1','管理员姓名1'),(102,'2022-02-11 12:41:51','物资名称2','口罩','upload/fangyiwuzi_wuzitupian2.jpg','物资规格2','物资品牌2',2,'物资介绍2','2022-02-11 20:41:51','管理员账号2','管理员姓名2'),(103,'2022-02-11 12:41:51','物资名称3','口罩','upload/fangyiwuzi_wuzitupian3.jpg','物资规格3','物资品牌3',3,'物资介绍3','2022-02-11 20:41:51','管理员账号3','管理员姓名3'),(104,'2022-02-11 12:41:51','物资名称4','口罩','upload/fangyiwuzi_wuzitupian4.jpg','物资规格4','物资品牌4',4,'物资介绍4','2022-02-11 20:41:51','管理员账号4','管理员姓名4'),(105,'2022-02-11 12:41:51','物资名称5','口罩','upload/fangyiwuzi_wuzitupian5.jpg','物资规格5','物资品牌5',5,'物资介绍5','2022-02-11 20:41:51','管理员账号5','管理员姓名5'),(106,'2022-02-11 12:41:51','物资名称6','口罩','upload/fangyiwuzi_wuzitupian6.jpg','物资规格6','物资品牌6',6,'物资介绍6','2022-02-11 20:41:51','管理员账号6','管理员姓名6');
/*!40000 ALTER TABLE `fangyiwuzi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fanxiangguanliyuan`
--

DROP TABLE IF EXISTS `fanxiangguanliyuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fanxiangguanliyuan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `guanliyuanzhanghao` varchar(200) NOT NULL COMMENT '管理员账号',
  `mima` varchar(200) NOT NULL COMMENT '密码',
  `guanliyuanxingming` varchar(200) NOT NULL COMMENT '管理员姓名',
  `xingbie` varchar(200) DEFAULT NULL COMMENT '性别',
  `lianxifangshi` varchar(200) DEFAULT NULL COMMENT '联系方式',
  PRIMARY KEY (`id`),
  UNIQUE KEY `guanliyuanzhanghao` (`guanliyuanzhanghao`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='返乡管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fanxiangguanliyuan`
--

LOCK TABLES `fanxiangguanliyuan` WRITE;
/*!40000 ALTER TABLE `fanxiangguanliyuan` DISABLE KEYS */;
INSERT INTO `fanxiangguanliyuan` VALUES (11,'2022-02-11 12:41:51','管理员账号1','123456','管理员姓名1','男','13823888881'),(12,'2022-02-11 12:41:51','管理员账号2','123456','管理员姓名2','男','13823888882'),(13,'2022-02-11 12:41:51','管理员账号3','123456','管理员姓名3','男','13823888883'),(14,'2022-02-11 12:41:51','管理员账号4','123456','管理员姓名4','男','13823888884'),(15,'2022-02-11 12:41:51','管理员账号5','123456','管理员姓名5','男','13823888885'),(16,'2022-02-11 12:41:51','管理员账号6','123456','管理员姓名6','男','13823888886');
/*!40000 ALTER TABLE `fanxiangguanliyuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fengxiandiqu`
--

DROP TABLE IF EXISTS `fengxiandiqu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fengxiandiqu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `diquleixing` varchar(200) DEFAULT NULL COMMENT '地区类型',
  `xianyouquezhen` varchar(200) DEFAULT NULL COMMENT '现有确诊',
  `leijiquezhen` varchar(200) DEFAULT NULL COMMENT '累计确诊',
  `siwangrenshu` varchar(200) DEFAULT NULL COMMENT '死亡人数',
  `zhiyurenshu` varchar(200) DEFAULT NULL COMMENT '治愈人数',
  `beizhu` longtext COMMENT '备注',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  `gengxinshijian` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1644583676794 DEFAULT CHARSET=utf8 COMMENT='风险地区';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fengxiandiqu`
--

LOCK TABLES `fengxiandiqu` WRITE;
/*!40000 ALTER TABLE `fengxiandiqu` DISABLE KEYS */;
INSERT INTO `fengxiandiqu` VALUES (71,'2022-02-11 12:41:51','低风险','现有确诊1','累计确诊1','死亡人数1','治愈人数1','备注1','管理员账号1','管理员姓名1','2022-02-11 20:41:51'),(72,'2022-02-11 12:41:51','低风险','现有确诊2','累计确诊2','死亡人数2','治愈人数2','备注2','管理员账号2','管理员姓名2','2022-02-11 20:41:51'),(73,'2022-02-11 12:41:51','低风险','现有确诊3','累计确诊3','死亡人数3','治愈人数3','备注3','管理员账号3','管理员姓名3','2022-02-11 20:41:51'),(74,'2022-02-11 12:41:51','低风险','现有确诊4','累计确诊4','死亡人数4','治愈人数4','备注4','管理员账号4','管理员姓名4','2022-02-11 20:41:51'),(75,'2022-02-11 12:41:51','低风险','现有确诊5','累计确诊5','死亡人数5','治愈人数5','备注5','管理员账号5','管理员姓名5','2022-02-11 20:41:51'),(76,'2022-02-11 12:41:51','低风险','现有确诊6','累计确诊6','死亡人数6','治愈人数6','备注6','管理员账号6','管理员姓名6','2022-02-11 20:41:51'),(1644583676793,'2022-02-11 12:47:56','低风险','2','1','5','2','2','111','242',NULL);
/*!40000 ALTER TABLE `fengxiandiqu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `geliguanliyuan`
--

DROP TABLE IF EXISTS `geliguanliyuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `geliguanliyuan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `guanliyuanzhanghao` varchar(200) NOT NULL COMMENT '管理员账号',
  `mima` varchar(200) NOT NULL COMMENT '密码',
  `guanliyuanxingming` varchar(200) NOT NULL COMMENT '管理员姓名',
  `xingbie` varchar(200) DEFAULT NULL COMMENT '性别',
  `lianxifangshi` varchar(200) DEFAULT NULL COMMENT '联系方式',
  PRIMARY KEY (`id`),
  UNIQUE KEY `guanliyuanzhanghao` (`guanliyuanzhanghao`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8 COMMENT='隔离管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `geliguanliyuan`
--

LOCK TABLES `geliguanliyuan` WRITE;
/*!40000 ALTER TABLE `geliguanliyuan` DISABLE KEYS */;
INSERT INTO `geliguanliyuan` VALUES (21,'2022-02-11 12:41:51','管理员账号1','123456','管理员姓名1','男','13823888881'),(22,'2022-02-11 12:41:51','管理员账号2','123456','管理员姓名2','男','13823888882'),(23,'2022-02-11 12:41:51','管理员账号3','123456','管理员姓名3','男','13823888883'),(24,'2022-02-11 12:41:51','管理员账号4','123456','管理员姓名4','男','13823888884'),(25,'2022-02-11 12:41:51','管理员账号5','123456','管理员姓名5','男','13823888885'),(26,'2022-02-11 12:41:51','管理员账号6','123456','管理员姓名6','男','13823888886');
/*!40000 ALTER TABLE `geliguanliyuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gonggaoxinxi`
--

DROP TABLE IF EXISTS `gonggaoxinxi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gonggaoxinxi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `biaoti` varchar(200) DEFAULT NULL COMMENT '标题',
  `jianjie` longtext COMMENT '简介',
  `fengmian` varchar(200) DEFAULT NULL COMMENT '封面',
  `faburiqi` datetime DEFAULT NULL COMMENT '发布日期',
  `neirong` longtext COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=127 DEFAULT CHARSET=utf8 COMMENT='公告信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gonggaoxinxi`
--

LOCK TABLES `gonggaoxinxi` WRITE;
/*!40000 ALTER TABLE `gonggaoxinxi` DISABLE KEYS */;
INSERT INTO `gonggaoxinxi` VALUES (121,'2022-02-11 12:41:51','标题1','简介1','upload/gonggaoxinxi_fengmian1.jpg','2022-02-11 20:41:51','内容1'),(122,'2022-02-11 12:41:51','标题2','简介2','upload/gonggaoxinxi_fengmian2.jpg','2022-02-11 20:41:51','内容2'),(123,'2022-02-11 12:41:51','标题3','简介3','upload/gonggaoxinxi_fengmian3.jpg','2022-02-11 20:41:51','内容3'),(124,'2022-02-11 12:41:51','标题4','简介4','upload/gonggaoxinxi_fengmian4.jpg','2022-02-11 20:41:51','内容4'),(125,'2022-02-11 12:41:51','标题5','简介5','upload/gonggaoxinxi_fengmian5.jpg','2022-02-11 20:41:51','内容5'),(126,'2022-02-11 12:41:51','标题6','简介6','upload/gonggaoxinxi_fengmian6.jpg','2022-02-11 20:41:51','内容6');
/*!40000 ALTER TABLE `gonggaoxinxi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hesuanjiance`
--

DROP TABLE IF EXISTS `hesuanjiance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hesuanjiance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `yonghuzhanghao` varchar(200) DEFAULT NULL COMMENT '用户账号',
  `yonghuxingming` varchar(200) DEFAULT NULL COMMENT '用户姓名',
  `zhuzhi` varchar(200) DEFAULT NULL COMMENT '住址',
  `hesuanbaogao` varchar(200) DEFAULT NULL COMMENT '核酸报告',
  `hesuanjieguo` varchar(200) DEFAULT NULL COMMENT '核酸结果',
  `jianceriqi` date DEFAULT NULL COMMENT '检测日期',
  `beizhu` longtext COMMENT '备注',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8 COMMENT='核酸检测';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hesuanjiance`
--

LOCK TABLES `hesuanjiance` WRITE;
/*!40000 ALTER TABLE `hesuanjiance` DISABLE KEYS */;
INSERT INTO `hesuanjiance` VALUES (91,'2022-02-11 12:41:51','用户账号1','用户姓名1','住址1','upload/hesuanjiance_hesuanbaogao1.jpg','阳性','2022-02-11','备注1','管理员账号1','管理员姓名1'),(92,'2022-02-11 12:41:51','用户账号2','用户姓名2','住址2','upload/hesuanjiance_hesuanbaogao2.jpg','阳性','2022-02-11','备注2','管理员账号2','管理员姓名2'),(93,'2022-02-11 12:41:51','用户账号3','用户姓名3','住址3','upload/hesuanjiance_hesuanbaogao3.jpg','阳性','2022-02-11','备注3','管理员账号3','管理员姓名3'),(94,'2022-02-11 12:41:51','用户账号4','用户姓名4','住址4','upload/hesuanjiance_hesuanbaogao4.jpg','阳性','2022-02-11','备注4','管理员账号4','管理员姓名4'),(95,'2022-02-11 12:41:51','用户账号5','用户姓名5','住址5','upload/hesuanjiance_hesuanbaogao5.jpg','阳性','2022-02-11','备注5','管理员账号5','管理员姓名5'),(96,'2022-02-11 12:41:51','用户账号6','用户姓名6','住址6','upload/hesuanjiance_hesuanbaogao6.jpg','阳性','2022-02-11','备注6','管理员账号6','管理员姓名6');
/*!40000 ALTER TABLE `hesuanjiance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jujiageli`
--

DROP TABLE IF EXISTS `jujiageli`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jujiageli` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `yonghuzhanghao` varchar(200) DEFAULT NULL COMMENT '用户账号',
  `yonghuxingming` varchar(200) DEFAULT NULL COMMENT '用户姓名',
  `zhuzhi` varchar(200) DEFAULT NULL COMMENT '住址',
  `gelishijian` date DEFAULT NULL COMMENT '隔离时间',
  `gelitianshu` varchar(200) DEFAULT NULL COMMENT '隔离天数',
  `jianceqingkuang` longtext COMMENT '检测情况',
  `dengjishijian` datetime DEFAULT NULL COMMENT '登记时间',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COMMENT='居家隔离';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jujiageli`
--

LOCK TABLES `jujiageli` WRITE;
/*!40000 ALTER TABLE `jujiageli` DISABLE KEYS */;
INSERT INTO `jujiageli` VALUES (81,'2022-02-11 12:41:51','用户账号1','用户姓名1','住址1','2022-02-11','隔离天数1','检测情况1','2022-02-11 20:41:51','管理员账号1','管理员姓名1'),(82,'2022-02-11 12:41:51','用户账号2','用户姓名2','住址2','2022-02-11','隔离天数2','检测情况2','2022-02-11 20:41:51','管理员账号2','管理员姓名2'),(83,'2022-02-11 12:41:51','用户账号3','用户姓名3','住址3','2022-02-11','隔离天数3','检测情况3','2022-02-11 20:41:51','管理员账号3','管理员姓名3'),(84,'2022-02-11 12:41:51','用户账号4','用户姓名4','住址4','2022-02-11','隔离天数4','检测情况4','2022-02-11 20:41:51','管理员账号4','管理员姓名4'),(85,'2022-02-11 12:41:51','用户账号5','用户姓名5','住址5','2022-02-11','隔离天数5','检测情况5','2022-02-11 20:41:51','管理员账号5','管理员姓名5'),(86,'2022-02-11 12:41:51','用户账号6','用户姓名6','住址6','2022-02-11','隔离天数6','检测情况6','2022-02-11 20:41:51','管理员账号6','管理员姓名6');
/*!40000 ALTER TABLE `jujiageli` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `quyuxinxi`
--

DROP TABLE IF EXISTS `quyuxinxi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `quyuxinxi` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `shengfen` varchar(200) DEFAULT NULL COMMENT '省份',
  `shixian` varchar(200) DEFAULT NULL COMMENT '市县',
  `zhenqu` varchar(200) DEFAULT NULL COMMENT '镇区',
  `quyubianhao` varchar(200) DEFAULT NULL COMMENT '区域编号',
  `quyumingcheng` varchar(200) DEFAULT NULL COMMENT '区域名称',
  `yonghuzhanghao` varchar(200) DEFAULT NULL COMMENT '用户账号',
  `yonghuxingming` varchar(200) DEFAULT NULL COMMENT '用户姓名',
  `lianxifangshi` varchar(200) DEFAULT NULL COMMENT '联系方式',
  `dengjishijian` datetime DEFAULT NULL COMMENT '登记时间',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8 COMMENT='区域信息';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `quyuxinxi`
--

LOCK TABLES `quyuxinxi` WRITE;
/*!40000 ALTER TABLE `quyuxinxi` DISABLE KEYS */;
INSERT INTO `quyuxinxi` VALUES (51,'2022-02-11 12:41:51','省份1','市县1','镇区1','区域编号1','区域名称1','用户账号1','用户姓名1','联系方式1','2022-02-11 20:41:51','管理员账号1','管理员姓名1'),(52,'2022-02-11 12:41:51','省份2','市县2','镇区2','区域编号2','区域名称2','用户账号2','用户姓名2','联系方式2','2022-02-11 20:41:51','管理员账号2','管理员姓名2'),(53,'2022-02-11 12:41:51','省份3','市县3','镇区3','区域编号3','区域名称3','用户账号3','用户姓名3','联系方式3','2022-02-11 20:41:51','管理员账号3','管理员姓名3'),(54,'2022-02-11 12:41:51','省份4','市县4','镇区4','区域编号4','区域名称4','用户账号4','用户姓名4','联系方式4','2022-02-11 20:41:51','管理员账号4','管理员姓名4'),(55,'2022-02-11 12:41:51','省份5','市县5','镇区5','区域编号5','区域名称5','用户账号5','用户姓名5','联系方式5','2022-02-11 20:41:51','管理员账号5','管理员姓名5'),(56,'2022-02-11 12:41:51','省份6','市县6','镇区6','区域编号6','区域名称6','用户账号6','用户姓名6','联系方式6','2022-02-11 20:41:51','管理员账号6','管理员姓名6');
/*!40000 ALTER TABLE `quyuxinxi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `renyuandengji`
--

DROP TABLE IF EXISTS `renyuandengji`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `renyuandengji` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `chufadidian` varchar(200) DEFAULT NULL COMMENT '出发地点',
  `yonghuzhanghao` varchar(200) DEFAULT NULL COMMENT '用户账号',
  `yonghuxingming` varchar(200) DEFAULT NULL COMMENT '用户姓名',
  `lianxifangshi` varchar(200) DEFAULT NULL COMMENT '联系方式',
  `zhuzhi` varchar(200) DEFAULT NULL COMMENT '住址',
  `jiankangma` varchar(200) DEFAULT NULL COMMENT '健康码',
  `shentizhuangtai` varchar(200) DEFAULT NULL COMMENT '身体状态',
  `fengxiandiqu` varchar(200) DEFAULT NULL COMMENT '风险地区',
  `hesuanjiance` varchar(200) DEFAULT NULL COMMENT '核酸检测',
  `jiancejieguo` varchar(200) DEFAULT NULL COMMENT '检测结果',
  `jianceshijian` datetime DEFAULT NULL COMMENT '检测时间',
  `fanxiangshijian` datetime DEFAULT NULL COMMENT '返乡时间',
  `dengjiriqi` datetime DEFAULT NULL COMMENT '登记日期',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COMMENT='人员登记';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `renyuandengji`
--

LOCK TABLES `renyuandengji` WRITE;
/*!40000 ALTER TABLE `renyuandengji` DISABLE KEYS */;
INSERT INTO `renyuandengji` VALUES (61,'2022-02-11 12:41:51','出发地点1','用户账号1','用户姓名1','联系方式1','住址1','upload/renyuandengji_jiankangma1.jpg','良好','低分险','是','阴性','2022-02-11 20:41:51','2022-02-11 20:41:51','2022-02-11 20:41:51','管理员账号1','管理员姓名1'),(62,'2022-02-11 12:41:51','出发地点2','用户账号2','用户姓名2','联系方式2','住址2','upload/renyuandengji_jiankangma2.jpg','良好','低分险','是','阴性','2022-02-11 20:41:51','2022-02-11 20:41:51','2022-02-11 20:41:51','管理员账号2','管理员姓名2'),(63,'2022-02-11 12:41:51','出发地点3','用户账号3','用户姓名3','联系方式3','住址3','upload/renyuandengji_jiankangma3.jpg','良好','低分险','是','阴性','2022-02-11 20:41:51','2022-02-11 20:41:51','2022-02-11 20:41:51','管理员账号3','管理员姓名3'),(64,'2022-02-11 12:41:51','出发地点4','用户账号4','用户姓名4','联系方式4','住址4','upload/renyuandengji_jiankangma4.jpg','良好','低分险','是','阴性','2022-02-11 20:41:51','2022-02-11 20:41:51','2022-02-11 20:41:51','管理员账号4','管理员姓名4'),(65,'2022-02-11 12:41:51','出发地点5','用户账号5','用户姓名5','联系方式5','住址5','upload/renyuandengji_jiankangma5.jpg','良好','低分险','是','阴性','2022-02-11 20:41:51','2022-02-11 20:41:51','2022-02-11 20:41:51','管理员账号5','管理员姓名5'),(66,'2022-02-11 12:41:51','出发地点6','用户账号6','用户姓名6','联系方式6','住址6','upload/renyuandengji_jiankangma6.jpg','良好','低分险','是','阴性','2022-02-11 20:41:51','2022-02-11 20:41:51','2022-02-11 20:41:51','管理员账号6','管理员姓名6');
/*!40000 ALTER TABLE `renyuandengji` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rizhijilu`
--

DROP TABLE IF EXISTS `rizhijilu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rizhijilu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `biaoti` varchar(200) DEFAULT NULL COMMENT '标题',
  `tupian` varchar(200) DEFAULT NULL COMMENT '图片',
  `jiluriqi` date DEFAULT NULL COMMENT '记录日期',
  `neirong` longtext COMMENT '内容',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=utf8 COMMENT='日志记录';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rizhijilu`
--

LOCK TABLES `rizhijilu` WRITE;
/*!40000 ALTER TABLE `rizhijilu` DISABLE KEYS */;
INSERT INTO `rizhijilu` VALUES (131,'2022-02-11 12:41:51','标题1','upload/rizhijilu_tupian1.jpg','2022-02-11','内容1'),(132,'2022-02-11 12:41:51','标题2','upload/rizhijilu_tupian2.jpg','2022-02-11','内容2'),(133,'2022-02-11 12:41:51','标题3','upload/rizhijilu_tupian3.jpg','2022-02-11','内容3'),(134,'2022-02-11 12:41:51','标题4','upload/rizhijilu_tupian4.jpg','2022-02-11','内容4'),(135,'2022-02-11 12:41:51','标题5','upload/rizhijilu_tupian5.jpg','2022-02-11','内容5'),(136,'2022-02-11 12:41:51','标题6','upload/rizhijilu_tupian6.jpg','2022-02-11','内容6');
/*!40000 ALTER TABLE `rizhijilu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `token`
--

DROP TABLE IF EXISTS `token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `token` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `userid` bigint(20) NOT NULL COMMENT '用户id',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `tablename` varchar(100) DEFAULT NULL COMMENT '表名',
  `role` varchar(100) DEFAULT NULL COMMENT '角色',
  `token` varchar(200) NOT NULL COMMENT '密码',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  `expiratedtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '过期时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='token表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `token`
--

LOCK TABLES `token` WRITE;
/*!40000 ALTER TABLE `token` DISABLE KEYS */;
INSERT INTO `token` VALUES (1,1,'abo','users','管理员','v5l7mc7rgr14ax58n2smtgpbjlkyvv3r','2022-02-11 12:42:39','2022-02-11 13:42:40');
/*!40000 ALTER TABLE `token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `username` varchar(100) NOT NULL COMMENT '用户名',
  `password` varchar(100) NOT NULL COMMENT '密码',
  `role` varchar(100) DEFAULT '管理员' COMMENT '角色',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '新增时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'abo','abo','管理员','2022-02-11 12:41:51');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wuzifenfa`
--

DROP TABLE IF EXISTS `wuzifenfa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wuzifenfa` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `wuzimingcheng` varchar(200) DEFAULT NULL COMMENT '物资名称',
  `wuzifenlei` varchar(200) DEFAULT NULL COMMENT '物资分类',
  `yonghuzhanghao` varchar(200) DEFAULT NULL COMMENT '用户账号',
  `yonghuxingming` varchar(200) DEFAULT NULL COMMENT '用户姓名',
  `zhuzhi` varchar(200) DEFAULT NULL COMMENT '住址',
  `wuzishuliang` int(11) DEFAULT NULL COMMENT '物资数量',
  `fenfariqi` datetime DEFAULT NULL COMMENT '分发日期',
  `beizhu` longtext COMMENT '备注',
  `guanliyuanzhanghao` varchar(200) DEFAULT NULL COMMENT '管理员账号',
  `guanliyuanxingming` varchar(200) DEFAULT NULL COMMENT '管理员姓名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8 COMMENT='物资分发';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wuzifenfa`
--

LOCK TABLES `wuzifenfa` WRITE;
/*!40000 ALTER TABLE `wuzifenfa` DISABLE KEYS */;
INSERT INTO `wuzifenfa` VALUES (111,'2022-02-11 12:41:51','物资名称1','物资分类1','用户账号1','用户姓名1','住址1',1,'2022-02-11 20:41:51','备注1','管理员账号1','管理员姓名1'),(112,'2022-02-11 12:41:51','物资名称2','物资分类2','用户账号2','用户姓名2','住址2',2,'2022-02-11 20:41:51','备注2','管理员账号2','管理员姓名2'),(113,'2022-02-11 12:41:51','物资名称3','物资分类3','用户账号3','用户姓名3','住址3',3,'2022-02-11 20:41:51','备注3','管理员账号3','管理员姓名3'),(114,'2022-02-11 12:41:51','物资名称4','物资分类4','用户账号4','用户姓名4','住址4',4,'2022-02-11 20:41:51','备注4','管理员账号4','管理员姓名4'),(115,'2022-02-11 12:41:51','物资名称5','物资分类5','用户账号5','用户姓名5','住址5',5,'2022-02-11 20:41:51','备注5','管理员账号5','管理员姓名5'),(116,'2022-02-11 12:41:51','物资名称6','物资分类6','用户账号6','用户姓名6','住址6',6,'2022-02-11 20:41:51','备注6','管理员账号6','管理员姓名6');
/*!40000 ALTER TABLE `wuzifenfa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wuziguanliyuan`
--

DROP TABLE IF EXISTS `wuziguanliyuan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `wuziguanliyuan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `guanliyuanzhanghao` varchar(200) NOT NULL COMMENT '管理员账号',
  `mima` varchar(200) NOT NULL COMMENT '密码',
  `guanliyuanxingming` varchar(200) NOT NULL COMMENT '管理员姓名',
  `xingbie` varchar(200) DEFAULT NULL COMMENT '性别',
  `lianxifangshi` varchar(200) DEFAULT NULL COMMENT '联系方式',
  PRIMARY KEY (`id`),
  UNIQUE KEY `guanliyuanzhanghao` (`guanliyuanzhanghao`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COMMENT='物资管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wuziguanliyuan`
--

LOCK TABLES `wuziguanliyuan` WRITE;
/*!40000 ALTER TABLE `wuziguanliyuan` DISABLE KEYS */;
INSERT INTO `wuziguanliyuan` VALUES (31,'2022-02-11 12:41:51','管理员账号1','123456','管理员姓名1','男','13823888881'),(32,'2022-02-11 12:41:51','管理员账号2','123456','管理员姓名2','男','13823888882'),(33,'2022-02-11 12:41:51','管理员账号3','123456','管理员姓名3','男','13823888883'),(34,'2022-02-11 12:41:51','管理员账号4','123456','管理员姓名4','男','13823888884'),(35,'2022-02-11 12:41:51','管理员账号5','123456','管理员姓名5','男','13823888885'),(36,'2022-02-11 12:41:51','管理员账号6','123456','管理员姓名6','男','13823888886');
/*!40000 ALTER TABLE `wuziguanliyuan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `yonghu`
--

DROP TABLE IF EXISTS `yonghu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `yonghu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `addtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `yonghuzhanghao` varchar(200) NOT NULL COMMENT '用户账号',
  `mima` varchar(200) NOT NULL COMMENT '密码',
  `yonghuxingming` varchar(200) NOT NULL COMMENT '用户姓名',
  `xingbie` varchar(200) DEFAULT NULL COMMENT '性别',
  `nianling` int(11) DEFAULT NULL COMMENT '年龄',
  `touxiang` varchar(200) DEFAULT NULL COMMENT '头像',
  `lianxifangshi` varchar(200) DEFAULT NULL COMMENT '联系方式',
  `zhuzhi` varchar(200) DEFAULT NULL COMMENT '住址',
  PRIMARY KEY (`id`),
  UNIQUE KEY `yonghuzhanghao` (`yonghuzhanghao`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `yonghu`
--

LOCK TABLES `yonghu` WRITE;
/*!40000 ALTER TABLE `yonghu` DISABLE KEYS */;
INSERT INTO `yonghu` VALUES (41,'2022-02-11 12:41:51','用户账号1','123456','用户姓名1','男',1,'upload/yonghu_touxiang1.jpg','13823888881','住址1'),(42,'2022-02-11 12:41:51','用户账号2','123456','用户姓名2','男',2,'upload/yonghu_touxiang2.jpg','13823888882','住址2'),(43,'2022-02-11 12:41:51','用户账号3','123456','用户姓名3','男',3,'upload/yonghu_touxiang3.jpg','13823888883','住址3'),(44,'2022-02-11 12:41:51','用户账号4','123456','用户姓名4','男',4,'upload/yonghu_touxiang4.jpg','13823888884','住址4'),(45,'2022-02-11 12:41:51','用户账号5','123456','用户姓名5','男',5,'upload/yonghu_touxiang5.jpg','13823888885','住址5'),(46,'2022-02-11 12:41:51','用户账号6','123456','用户姓名6','男',6,'upload/yonghu_touxiang6.jpg','13823888886','住址6');
/*!40000 ALTER TABLE `yonghu` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-02-12 16:17:05
