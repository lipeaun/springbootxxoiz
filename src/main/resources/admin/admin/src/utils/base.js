const base = {
    get() {
        return {
            url : "http://localhost:8080/springbootxxoiz/",
            name: "springbootxxoiz",
            // 退出到首页链接
            indexUrl: ''
        };
    },
    getProjectName(){
        return {
            projectName: "县返乡人员防疫管理平台"
        } 
    }
}
export default base
