import Vue from 'vue';
//配置路由
import VueRouter from 'vue-router'
Vue.use(VueRouter);
//1.创建组件
import Index from '@/views/index'
import Home from '@/views/home'
import Login from '@/views/login'
import NotFound from '@/views/404'
import UpdatePassword from '@/views/update-password'
import pay from '@/views/pay'
import register from '@/views/register'
import center from '@/views/center'
    import wuziguanliyuan from '@/views/modules/wuziguanliyuan/list'
    import renyuandengji from '@/views/modules/renyuandengji/list'
    import fengxiandiqu from '@/views/modules/fengxiandiqu/list'
    import quyuxinxi from '@/views/modules/quyuxinxi/list'
    import fanxiangguanliyuan from '@/views/modules/fanxiangguanliyuan/list'
    import rizhijilu from '@/views/modules/rizhijilu/list'
    import yonghu from '@/views/modules/yonghu/list'
    import wuzifenfa from '@/views/modules/wuzifenfa/list'
    import geliguanliyuan from '@/views/modules/geliguanliyuan/list'
    import jujiageli from '@/views/modules/jujiageli/list'
    import hesuanjiance from '@/views/modules/hesuanjiance/list'
    import gonggaoxinxi from '@/views/modules/gonggaoxinxi/list'
    import fangyiwuzi from '@/views/modules/fangyiwuzi/list'


//2.配置路由   注意：名字
const routes = [{
    path: '/index',
    name: '首页',
    component: Index,
    children: [{
      // 这里不设置值，是把main作为默认页面
      path: '/',
      name: '首页',
      component: Home,
      meta: {icon:'', title:'center'}
    }, {
      path: '/updatePassword',
      name: '修改密码',
      component: UpdatePassword,
      meta: {icon:'', title:'updatePassword'}
    }, {
      path: '/pay',
      name: '支付',
      component: pay,
      meta: {icon:'', title:'pay'}
    }, {
      path: '/center',
      name: '个人信息',
      component: center,
      meta: {icon:'', title:'center'}
    }
      ,{
	path: '/wuziguanliyuan',
        name: '物资管理员',
        component: wuziguanliyuan
      }
      ,{
	path: '/renyuandengji',
        name: '人员登记',
        component: renyuandengji
      }
      ,{
	path: '/fengxiandiqu',
        name: '风险地区',
        component: fengxiandiqu
      }
      ,{
	path: '/quyuxinxi',
        name: '区域信息',
        component: quyuxinxi
      }
      ,{
	path: '/fanxiangguanliyuan',
        name: '返乡管理员',
        component: fanxiangguanliyuan
      }
      ,{
	path: '/rizhijilu',
        name: '日志记录',
        component: rizhijilu
      }
      ,{
	path: '/yonghu',
        name: '用户',
        component: yonghu
      }
      ,{
	path: '/wuzifenfa',
        name: '物资分发',
        component: wuzifenfa
      }
      ,{
	path: '/geliguanliyuan',
        name: '隔离管理员',
        component: geliguanliyuan
      }
      ,{
	path: '/jujiageli',
        name: '居家隔离',
        component: jujiageli
      }
      ,{
	path: '/hesuanjiance',
        name: '核酸检测',
        component: hesuanjiance
      }
      ,{
	path: '/gonggaoxinxi',
        name: '公告信息',
        component: gonggaoxinxi
      }
      ,{
	path: '/fangyiwuzi',
        name: '防疫物资',
        component: fangyiwuzi
      }
    ]
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {icon:'', title:'login'}
  },
  {
    path: '/register',
    name: 'register',
    component: register,
    meta: {icon:'', title:'register'}
  },
  {
    path: '/',
    name: '首页',
    redirect: '/index'
  }, /*默认跳转路由*/
  {
    path: '*',
    component: NotFound
  }
]
//3.实例化VueRouter  注意：名字
const router = new VueRouter({
  mode: 'hash',
  /*hash模式改为history*/
  routes // （缩写）相当于 routes: routes
})

export default router;
