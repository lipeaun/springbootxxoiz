package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.FengxiandiquEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.FengxiandiquView;


/**
 * 风险地区
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface FengxiandiquService extends IService<FengxiandiquEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<FengxiandiquView> selectListView(Wrapper<FengxiandiquEntity> wrapper);
   	
   	FengxiandiquView selectView(@Param("ew") Wrapper<FengxiandiquEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<FengxiandiquEntity> wrapper);
   	

    List<Map<String, Object>> selectValue(Map<String, Object> params,Wrapper<FengxiandiquEntity> wrapper);

    List<Map<String, Object>> selectTimeStatValue(Map<String, Object> params,Wrapper<FengxiandiquEntity> wrapper);

    List<Map<String, Object>> selectGroup(Map<String, Object> params,Wrapper<FengxiandiquEntity> wrapper);
}

