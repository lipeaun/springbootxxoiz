package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.FangyiwuziEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.FangyiwuziView;


/**
 * 防疫物资
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface FangyiwuziService extends IService<FangyiwuziEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<FangyiwuziView> selectListView(Wrapper<FangyiwuziEntity> wrapper);
   	
   	FangyiwuziView selectView(@Param("ew") Wrapper<FangyiwuziEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<FangyiwuziEntity> wrapper);
   	

}

