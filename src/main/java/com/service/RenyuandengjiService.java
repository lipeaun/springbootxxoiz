package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.RenyuandengjiEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.RenyuandengjiView;


/**
 * 人员登记
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface RenyuandengjiService extends IService<RenyuandengjiEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<RenyuandengjiView> selectListView(Wrapper<RenyuandengjiEntity> wrapper);
   	
   	RenyuandengjiView selectView(@Param("ew") Wrapper<RenyuandengjiEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<RenyuandengjiEntity> wrapper);
   	

}

