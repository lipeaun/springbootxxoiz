package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.RizhijiluEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.RizhijiluView;


/**
 * 日志记录
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface RizhijiluService extends IService<RizhijiluEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<RizhijiluView> selectListView(Wrapper<RizhijiluEntity> wrapper);
   	
   	RizhijiluView selectView(@Param("ew") Wrapper<RizhijiluEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<RizhijiluEntity> wrapper);
   	

}

