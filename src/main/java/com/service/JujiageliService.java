package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.JujiageliEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.JujiageliView;


/**
 * 居家隔离
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface JujiageliService extends IService<JujiageliEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<JujiageliView> selectListView(Wrapper<JujiageliEntity> wrapper);
   	
   	JujiageliView selectView(@Param("ew") Wrapper<JujiageliEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<JujiageliEntity> wrapper);
   	

}

