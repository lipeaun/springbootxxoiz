package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.HesuanjianceEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.HesuanjianceView;


/**
 * 核酸检测
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface HesuanjianceService extends IService<HesuanjianceEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<HesuanjianceView> selectListView(Wrapper<HesuanjianceEntity> wrapper);
   	
   	HesuanjianceView selectView(@Param("ew") Wrapper<HesuanjianceEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<HesuanjianceEntity> wrapper);
   	

}

