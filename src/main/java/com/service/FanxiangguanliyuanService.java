package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.FanxiangguanliyuanEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.FanxiangguanliyuanView;


/**
 * 返乡管理员
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface FanxiangguanliyuanService extends IService<FanxiangguanliyuanEntity> {

    PageUtils queryPage(Map<String, Object> params);

   	List<FanxiangguanliyuanView> selectListView(Wrapper<FanxiangguanliyuanEntity> wrapper);
   	
   	FanxiangguanliyuanView selectView(@Param("ew") Wrapper<FanxiangguanliyuanEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<FanxiangguanliyuanEntity> wrapper);
   	

}

