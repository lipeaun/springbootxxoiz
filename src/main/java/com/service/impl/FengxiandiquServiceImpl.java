package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.FengxiandiquDao;
import com.model.FengxiandiquEntity;
import com.service.FengxiandiquService;
import com.model.web.FengxiandiquView;

@Service("fengxiandiquService")
public class FengxiandiquServiceImpl extends ServiceImpl<FengxiandiquDao, FengxiandiquEntity> implements FengxiandiquService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<FengxiandiquEntity> page = this.selectPage(
                new Query<FengxiandiquEntity>(params).getPage(),
                new EntityWrapper<FengxiandiquEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<FengxiandiquEntity> wrapper) {
		  Page<FengxiandiquView> page =new Query<FengxiandiquView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

	@Override
	public List<FengxiandiquView> selectListView(Wrapper<FengxiandiquEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public FengxiandiquView selectView(Wrapper<FengxiandiquEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

    @Override
    public List<Map<String, Object>> selectValue(Map<String, Object> params, Wrapper<FengxiandiquEntity> wrapper) {
        return baseMapper.selectValue(params, wrapper);
    }

    @Override
    public List<Map<String, Object>> selectTimeStatValue(Map<String, Object> params, Wrapper<FengxiandiquEntity> wrapper) {
        return baseMapper.selectTimeStatValue(params, wrapper);
    }

    @Override
    public List<Map<String, Object>> selectGroup(Map<String, Object> params, Wrapper<FengxiandiquEntity> wrapper) {
        return baseMapper.selectGroup(params, wrapper);
    }

}
