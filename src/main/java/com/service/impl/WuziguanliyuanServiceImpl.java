package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.WuziguanliyuanDao;
import com.model.WuziguanliyuanEntity;
import com.service.WuziguanliyuanService;
import com.model.web.WuziguanliyuanView;

@Service("wuziguanliyuanService")
public class WuziguanliyuanServiceImpl extends ServiceImpl<WuziguanliyuanDao, WuziguanliyuanEntity> implements WuziguanliyuanService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<WuziguanliyuanEntity> page = this.selectPage(
                new Query<WuziguanliyuanEntity>(params).getPage(),
                new EntityWrapper<WuziguanliyuanEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<WuziguanliyuanEntity> wrapper) {
		  Page<WuziguanliyuanView> page =new Query<WuziguanliyuanView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

	@Override
	public List<WuziguanliyuanView> selectListView(Wrapper<WuziguanliyuanEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public WuziguanliyuanView selectView(Wrapper<WuziguanliyuanEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
