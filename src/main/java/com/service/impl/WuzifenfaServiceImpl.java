package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.WuzifenfaDao;
import com.model.WuzifenfaEntity;
import com.service.WuzifenfaService;
import com.model.web.WuzifenfaView;

@Service("wuzifenfaService")
public class WuzifenfaServiceImpl extends ServiceImpl<WuzifenfaDao, WuzifenfaEntity> implements WuzifenfaService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<WuzifenfaEntity> page = this.selectPage(
                new Query<WuzifenfaEntity>(params).getPage(),
                new EntityWrapper<WuzifenfaEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<WuzifenfaEntity> wrapper) {
		  Page<WuzifenfaView> page =new Query<WuzifenfaView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

	@Override
	public List<WuzifenfaView> selectListView(Wrapper<WuzifenfaEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public WuzifenfaView selectView(Wrapper<WuzifenfaEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
