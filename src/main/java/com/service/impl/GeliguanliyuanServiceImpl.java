package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.GeliguanliyuanDao;
import com.model.GeliguanliyuanEntity;
import com.service.GeliguanliyuanService;
import com.model.web.GeliguanliyuanView;

@Service("geliguanliyuanService")
public class GeliguanliyuanServiceImpl extends ServiceImpl<GeliguanliyuanDao, GeliguanliyuanEntity> implements GeliguanliyuanService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<GeliguanliyuanEntity> page = this.selectPage(
                new Query<GeliguanliyuanEntity>(params).getPage(),
                new EntityWrapper<GeliguanliyuanEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<GeliguanliyuanEntity> wrapper) {
		  Page<GeliguanliyuanView> page =new Query<GeliguanliyuanView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

	@Override
	public List<GeliguanliyuanView> selectListView(Wrapper<GeliguanliyuanEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public GeliguanliyuanView selectView(Wrapper<GeliguanliyuanEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
