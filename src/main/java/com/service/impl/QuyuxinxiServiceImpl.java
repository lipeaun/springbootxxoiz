package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.QuyuxinxiDao;
import com.model.QuyuxinxiEntity;
import com.service.QuyuxinxiService;
import com.model.web.QuyuxinxiView;

@Service("quyuxinxiService")
public class QuyuxinxiServiceImpl extends ServiceImpl<QuyuxinxiDao, QuyuxinxiEntity> implements QuyuxinxiService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<QuyuxinxiEntity> page = this.selectPage(
                new Query<QuyuxinxiEntity>(params).getPage(),
                new EntityWrapper<QuyuxinxiEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<QuyuxinxiEntity> wrapper) {
		  Page<QuyuxinxiView> page =new Query<QuyuxinxiView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

	@Override
	public List<QuyuxinxiView> selectListView(Wrapper<QuyuxinxiEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public QuyuxinxiView selectView(Wrapper<QuyuxinxiEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}

    @Override
    public List<Map<String, Object>> selectValue(Map<String, Object> params, Wrapper<QuyuxinxiEntity> wrapper) {
        return baseMapper.selectValue(params, wrapper);
    }

    @Override
    public List<Map<String, Object>> selectTimeStatValue(Map<String, Object> params, Wrapper<QuyuxinxiEntity> wrapper) {
        return baseMapper.selectTimeStatValue(params, wrapper);
    }

    @Override
    public List<Map<String, Object>> selectGroup(Map<String, Object> params, Wrapper<QuyuxinxiEntity> wrapper) {
        return baseMapper.selectGroup(params, wrapper);
    }

}
