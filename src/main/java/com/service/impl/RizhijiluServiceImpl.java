package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.RizhijiluDao;
import com.model.RizhijiluEntity;
import com.service.RizhijiluService;
import com.model.web.RizhijiluView;

@Service("rizhijiluService")
public class RizhijiluServiceImpl extends ServiceImpl<RizhijiluDao, RizhijiluEntity> implements RizhijiluService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RizhijiluEntity> page = this.selectPage(
                new Query<RizhijiluEntity>(params).getPage(),
                new EntityWrapper<RizhijiluEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<RizhijiluEntity> wrapper) {
		  Page<RizhijiluView> page =new Query<RizhijiluView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    

	@Override
	public List<RizhijiluView> selectListView(Wrapper<RizhijiluEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public RizhijiluView selectView(Wrapper<RizhijiluEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
