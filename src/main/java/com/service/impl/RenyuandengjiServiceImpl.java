package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.RenyuandengjiDao;
import com.model.RenyuandengjiEntity;
import com.service.RenyuandengjiService;
import com.model.web.RenyuandengjiView;

@Service("renyuandengjiService")
public class RenyuandengjiServiceImpl extends ServiceImpl<RenyuandengjiDao, RenyuandengjiEntity> implements RenyuandengjiService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<RenyuandengjiEntity> page = this.selectPage(
                new Query<RenyuandengjiEntity>(params).getPage(),
                new EntityWrapper<RenyuandengjiEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<RenyuandengjiEntity> wrapper) {
		  Page<RenyuandengjiView> page =new Query<RenyuandengjiView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    

	@Override
	public List<RenyuandengjiView> selectListView(Wrapper<RenyuandengjiEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public RenyuandengjiView selectView(Wrapper<RenyuandengjiEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
