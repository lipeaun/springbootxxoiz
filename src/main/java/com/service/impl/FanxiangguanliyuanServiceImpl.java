package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.FanxiangguanliyuanDao;
import com.model.FanxiangguanliyuanEntity;
import com.service.FanxiangguanliyuanService;
import com.model.web.FanxiangguanliyuanView;

@Service("fanxiangguanliyuanService")
public class FanxiangguanliyuanServiceImpl extends ServiceImpl<FanxiangguanliyuanDao, FanxiangguanliyuanEntity> implements FanxiangguanliyuanService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<FanxiangguanliyuanEntity> page = this.selectPage(
                new Query<FanxiangguanliyuanEntity>(params).getPage(),
                new EntityWrapper<FanxiangguanliyuanEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<FanxiangguanliyuanEntity> wrapper) {
		  Page<FanxiangguanliyuanView> page =new Query<FanxiangguanliyuanView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}

	
	@Override
	public List<FanxiangguanliyuanView> selectListView(Wrapper<FanxiangguanliyuanEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public FanxiangguanliyuanView selectView(Wrapper<FanxiangguanliyuanEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
