package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.FangyiwuziDao;
import com.model.FangyiwuziEntity;
import com.service.FangyiwuziService;
import com.model.web.FangyiwuziView;

@Service("fangyiwuziService")
public class FangyiwuziServiceImpl extends ServiceImpl<FangyiwuziDao, FangyiwuziEntity> implements FangyiwuziService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<FangyiwuziEntity> page = this.selectPage(
                new Query<FangyiwuziEntity>(params).getPage(),
                new EntityWrapper<FangyiwuziEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<FangyiwuziEntity> wrapper) {
		  Page<FangyiwuziView> page =new Query<FangyiwuziView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    

	@Override
	public List<FangyiwuziView> selectListView(Wrapper<FangyiwuziEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public FangyiwuziView selectView(Wrapper<FangyiwuziEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
