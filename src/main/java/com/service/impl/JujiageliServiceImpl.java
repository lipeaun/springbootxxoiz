package com.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.utils.PageUtils;
import com.utils.Query;


import com.dao.JujiageliDao;
import com.model.JujiageliEntity;
import com.service.JujiageliService;
import com.model.web.JujiageliView;

@Service("jujiageliService")
public class JujiageliServiceImpl extends ServiceImpl<JujiageliDao, JujiageliEntity> implements JujiageliService {
	
	
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        Page<JujiageliEntity> page = this.selectPage(
                new Query<JujiageliEntity>(params).getPage(),
                new EntityWrapper<JujiageliEntity>()
        );
        return new PageUtils(page);
    }
    
    @Override
	public PageUtils queryPage(Map<String, Object> params, Wrapper<JujiageliEntity> wrapper) {
		  Page<JujiageliView> page =new Query<JujiageliView>(params).getPage();
	        page.setRecords(baseMapper.selectListView(page,wrapper));
	    	PageUtils pageUtil = new PageUtils(page);
	    	return pageUtil;
 	}
    

	@Override
	public List<JujiageliView> selectListView(Wrapper<JujiageliEntity> wrapper) {
		return baseMapper.selectListView(wrapper);
	}

	@Override
	public JujiageliView selectView(Wrapper<JujiageliEntity> wrapper) {
		return baseMapper.selectView(wrapper);
	}


}
