package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.WuzifenfaEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.WuzifenfaView;


/**
 * 物资分发
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface WuzifenfaService extends IService<WuzifenfaEntity> {

    PageUtils queryPage(Map<String, Object> params);

   	List<WuzifenfaView> selectListView(Wrapper<WuzifenfaEntity> wrapper);
   	
   	WuzifenfaView selectView(@Param("ew") Wrapper<WuzifenfaEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<WuzifenfaEntity> wrapper);
   	

}

