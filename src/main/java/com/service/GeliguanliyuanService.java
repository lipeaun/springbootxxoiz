package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.GeliguanliyuanEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.GeliguanliyuanView;


/**
 * 隔离管理员
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface GeliguanliyuanService extends IService<GeliguanliyuanEntity> {

    PageUtils queryPage(Map<String, Object> params);
    

   	List<GeliguanliyuanView> selectListView(Wrapper<GeliguanliyuanEntity> wrapper);
   	
   	GeliguanliyuanView selectView(@Param("ew") Wrapper<GeliguanliyuanEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<GeliguanliyuanEntity> wrapper);
   	

}

