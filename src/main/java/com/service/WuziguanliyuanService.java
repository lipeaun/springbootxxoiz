package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.WuziguanliyuanEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.WuziguanliyuanView;


/**
 * 物资管理员
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface WuziguanliyuanService extends IService<WuziguanliyuanEntity> {

    PageUtils queryPage(Map<String, Object> params);

   	List<WuziguanliyuanView> selectListView(Wrapper<WuziguanliyuanEntity> wrapper);
   	
   	WuziguanliyuanView selectView(@Param("ew") Wrapper<WuziguanliyuanEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<WuziguanliyuanEntity> wrapper);
   	

}

