package com.service;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.IService;
import com.utils.PageUtils;
import com.model.QuyuxinxiEntity;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import com.model.web.QuyuxinxiView;


/**
 * 区域信息
 *
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface QuyuxinxiService extends IService<QuyuxinxiEntity> {

    PageUtils queryPage(Map<String, Object> params);

   	List<QuyuxinxiView> selectListView(Wrapper<QuyuxinxiEntity> wrapper);
   	
   	QuyuxinxiView selectView(@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);
   	
   	PageUtils queryPage(Map<String, Object> params,Wrapper<QuyuxinxiEntity> wrapper);
   	

    List<Map<String, Object>> selectValue(Map<String, Object> params,Wrapper<QuyuxinxiEntity> wrapper);

    List<Map<String, Object>> selectTimeStatValue(Map<String, Object> params,Wrapper<QuyuxinxiEntity> wrapper);

    List<Map<String, Object>> selectGroup(Map<String, Object> params,Wrapper<QuyuxinxiEntity> wrapper);
}

