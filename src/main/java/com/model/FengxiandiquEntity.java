package com.model;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.beanutils.BeanUtils;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.enums.FieldFill;
import com.baomidou.mybatisplus.enums.IdType;


/**
 * 风险地区
 * 数据库通用操作实体类（普通增删改查）
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@TableName("fengxiandiqu")
public class FengxiandiquEntity<T> implements Serializable {
	private static final long serialVersionUID = 1L;


	public FengxiandiquEntity() {
		
	}
	
	public FengxiandiquEntity(T t) {
		try {
			BeanUtils.copyProperties(this, t);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 主键id
	 */
	@TableId
	private Long id;
	/**
	 * 地区类型
	 */
					
	private String diquleixing;
	
	/**
	 * 现有确诊
	 */
					
	private String xianyouquezhen;
	
	/**
	 * 累计确诊
	 */
					
	private String leijiquezhen;
	
	/**
	 * 死亡人数
	 */
					
	private String siwangrenshu;
	
	/**
	 * 治愈人数
	 */
					
	private String zhiyurenshu;
	
	/**
	 * 备注
	 */
					
	private String beizhu;
	
	/**
	 * 管理员账号
	 */
					
	private String guanliyuanzhanghao;
	
	/**
	 * 管理员姓名
	 */
					
	private String guanliyuanxingming;
	
	/**
	 * 更新时间
	 */
				
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat 		
	private Date gengxinshijian;
	
	
	@JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat
	private Date addtime;

	public Date getAddtime() {
		return addtime;
	}
	public void setAddtime(Date addtime) {
		this.addtime = addtime;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * 设置：地区类型
	 */
	public void setDiquleixing(String diquleixing) {
		this.diquleixing = diquleixing;
	}
	/**
	 * 获取：地区类型
	 */
	public String getDiquleixing() {
		return diquleixing;
	}
	/**
	 * 设置：现有确诊
	 */
	public void setXianyouquezhen(String xianyouquezhen) {
		this.xianyouquezhen = xianyouquezhen;
	}
	/**
	 * 获取：现有确诊
	 */
	public String getXianyouquezhen() {
		return xianyouquezhen;
	}
	/**
	 * 设置：累计确诊
	 */
	public void setLeijiquezhen(String leijiquezhen) {
		this.leijiquezhen = leijiquezhen;
	}
	/**
	 * 获取：累计确诊
	 */
	public String getLeijiquezhen() {
		return leijiquezhen;
	}
	/**
	 * 设置：死亡人数
	 */
	public void setSiwangrenshu(String siwangrenshu) {
		this.siwangrenshu = siwangrenshu;
	}
	/**
	 * 获取：死亡人数
	 */
	public String getSiwangrenshu() {
		return siwangrenshu;
	}
	/**
	 * 设置：治愈人数
	 */
	public void setZhiyurenshu(String zhiyurenshu) {
		this.zhiyurenshu = zhiyurenshu;
	}
	/**
	 * 获取：治愈人数
	 */
	public String getZhiyurenshu() {
		return zhiyurenshu;
	}
	/**
	 * 设置：备注
	 */
	public void setBeizhu(String beizhu) {
		this.beizhu = beizhu;
	}
	/**
	 * 获取：备注
	 */
	public String getBeizhu() {
		return beizhu;
	}
	/**
	 * 设置：管理员账号
	 */
	public void setGuanliyuanzhanghao(String guanliyuanzhanghao) {
		this.guanliyuanzhanghao = guanliyuanzhanghao;
	}
	/**
	 * 获取：管理员账号
	 */
	public String getGuanliyuanzhanghao() {
		return guanliyuanzhanghao;
	}
	/**
	 * 设置：管理员姓名
	 */
	public void setGuanliyuanxingming(String guanliyuanxingming) {
		this.guanliyuanxingming = guanliyuanxingming;
	}
	/**
	 * 获取：管理员姓名
	 */
	public String getGuanliyuanxingming() {
		return guanliyuanxingming;
	}
	/**
	 * 设置：更新时间
	 */
	public void setGengxinshijian(Date gengxinshijian) {
		this.gengxinshijian = gengxinshijian;
	}
	/**
	 * 获取：更新时间
	 */
	public Date getGengxinshijian() {
		return gengxinshijian;
	}

}
