package com.model.web;

import com.model.WuziguanliyuanEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 物资管理员
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@TableName("wuziguanliyuan")
public class WuziguanliyuanView  extends WuziguanliyuanEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public WuziguanliyuanView(){
	}
 
 	public WuziguanliyuanView(WuziguanliyuanEntity wuziguanliyuanEntity){
 	try {
			BeanUtils.copyProperties(this, wuziguanliyuanEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
