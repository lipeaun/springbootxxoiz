package com.model.web;

import com.model.JujiageliEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 居家隔离
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@TableName("jujiageli")
public class JujiageliView  extends JujiageliEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public JujiageliView(){
	}
 
 	public JujiageliView(JujiageliEntity jujiageliEntity){
 	try {
			BeanUtils.copyProperties(this, jujiageliEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
