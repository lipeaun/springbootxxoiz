package com.model.web;

import com.model.RizhijiluEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 日志记录
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@TableName("rizhijilu")
public class RizhijiluView  extends RizhijiluEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public RizhijiluView(){
	}
 
 	public RizhijiluView(RizhijiluEntity rizhijiluEntity){
 	try {
			BeanUtils.copyProperties(this, rizhijiluEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
