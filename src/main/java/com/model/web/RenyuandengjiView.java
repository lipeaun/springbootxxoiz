package com.model.web;

import com.model.RenyuandengjiEntity;

import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.commons.beanutils.BeanUtils;
import java.lang.reflect.InvocationTargetException;

import java.io.Serializable;
 

/**
 * 人员登记
 * 后端返回视图实体辅助类   
 * （通常后端关联的表或者自定义的字段需要返回使用）
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@TableName("renyuandengji")
public class RenyuandengjiView  extends RenyuandengjiEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	public RenyuandengjiView(){
	}
 
 	public RenyuandengjiView(RenyuandengjiEntity renyuandengjiEntity){
 	try {
			BeanUtils.copyProperties(this, renyuandengjiEntity);
		} catch (IllegalAccessException | InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 		
	}
}
