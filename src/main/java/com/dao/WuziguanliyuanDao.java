package com.dao;

import com.model.WuziguanliyuanEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.WuziguanliyuanView;


/**
 * 物资管理员
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface WuziguanliyuanDao extends BaseMapper<WuziguanliyuanEntity> {
	
	List<WuziguanliyuanView> selectListView(@Param("ew") Wrapper<WuziguanliyuanEntity> wrapper);

	List<WuziguanliyuanView> selectListView(Pagination page,@Param("ew") Wrapper<WuziguanliyuanEntity> wrapper);
	
	WuziguanliyuanView selectView(@Param("ew") Wrapper<WuziguanliyuanEntity> wrapper);
	

}
