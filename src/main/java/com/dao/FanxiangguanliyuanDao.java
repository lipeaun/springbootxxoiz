package com.dao;

import com.model.FanxiangguanliyuanEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.FanxiangguanliyuanView;


/**
 * 返乡管理员
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface FanxiangguanliyuanDao extends BaseMapper<FanxiangguanliyuanEntity> {

	List<FanxiangguanliyuanView> selectListView(@Param("ew") Wrapper<FanxiangguanliyuanEntity> wrapper);

	List<FanxiangguanliyuanView> selectListView(Pagination page,@Param("ew") Wrapper<FanxiangguanliyuanEntity> wrapper);
	
	FanxiangguanliyuanView selectView(@Param("ew") Wrapper<FanxiangguanliyuanEntity> wrapper);
	

}
