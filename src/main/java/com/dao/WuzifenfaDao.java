package com.dao;

import com.model.WuzifenfaEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.WuzifenfaView;


/**
 * 物资分发
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface WuzifenfaDao extends BaseMapper<WuzifenfaEntity> {
	

	List<WuzifenfaView> selectListView(@Param("ew") Wrapper<WuzifenfaEntity> wrapper);

	List<WuzifenfaView> selectListView(Pagination page,@Param("ew") Wrapper<WuzifenfaEntity> wrapper);
	
	WuzifenfaView selectView(@Param("ew") Wrapper<WuzifenfaEntity> wrapper);
	

}
