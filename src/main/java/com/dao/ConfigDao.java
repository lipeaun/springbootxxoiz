
package com.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.model.ConfigEntity;

/**
 * 配置
 */
public interface ConfigDao extends BaseMapper<ConfigEntity> {
	
}
