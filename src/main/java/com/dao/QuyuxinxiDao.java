package com.dao;

import com.model.QuyuxinxiEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.QuyuxinxiView;


/**
 * 区域信息
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface QuyuxinxiDao extends BaseMapper<QuyuxinxiEntity> {

	List<QuyuxinxiView> selectListView(@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);

	List<QuyuxinxiView> selectListView(Pagination page,@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);
	
	QuyuxinxiView selectView(@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);
	

    List<Map<String, Object>> selectValue(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);

    List<Map<String, Object>> selectTimeStatValue(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);

    List<Map<String, Object>> selectGroup(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<QuyuxinxiEntity> wrapper);
}
