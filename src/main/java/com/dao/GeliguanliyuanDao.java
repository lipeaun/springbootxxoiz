package com.dao;

import com.model.GeliguanliyuanEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.GeliguanliyuanView;


/**
 * 隔离管理员
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface GeliguanliyuanDao extends BaseMapper<GeliguanliyuanEntity> {

	List<GeliguanliyuanView> selectListView(@Param("ew") Wrapper<GeliguanliyuanEntity> wrapper);

	List<GeliguanliyuanView> selectListView(Pagination page,@Param("ew") Wrapper<GeliguanliyuanEntity> wrapper);
	
	GeliguanliyuanView selectView(@Param("ew") Wrapper<GeliguanliyuanEntity> wrapper);
	

}
