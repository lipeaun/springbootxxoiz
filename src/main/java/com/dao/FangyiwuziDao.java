package com.dao;

import com.model.FangyiwuziEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.FangyiwuziView;


/**
 * 防疫物资
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface FangyiwuziDao extends BaseMapper<FangyiwuziEntity> {

	
	List<FangyiwuziView> selectListView(@Param("ew") Wrapper<FangyiwuziEntity> wrapper);

	List<FangyiwuziView> selectListView(Pagination page,@Param("ew") Wrapper<FangyiwuziEntity> wrapper);
	
	FangyiwuziView selectView(@Param("ew") Wrapper<FangyiwuziEntity> wrapper);
	

}
