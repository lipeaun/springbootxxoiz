package com.dao;

import com.model.RizhijiluEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.RizhijiluView;


/**
 * 日志记录
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface RizhijiluDao extends BaseMapper<RizhijiluEntity> {
	

	List<RizhijiluView> selectListView(@Param("ew") Wrapper<RizhijiluEntity> wrapper);

	List<RizhijiluView> selectListView(Pagination page,@Param("ew") Wrapper<RizhijiluEntity> wrapper);
	
	RizhijiluView selectView(@Param("ew") Wrapper<RizhijiluEntity> wrapper);
	

}
