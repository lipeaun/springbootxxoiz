package com.dao;

import com.model.JujiageliEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.JujiageliView;


/**
 * 居家隔离
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface JujiageliDao extends BaseMapper<JujiageliEntity> {

	List<JujiageliView> selectListView(@Param("ew") Wrapper<JujiageliEntity> wrapper);

	List<JujiageliView> selectListView(Pagination page,@Param("ew") Wrapper<JujiageliEntity> wrapper);
	
	JujiageliView selectView(@Param("ew") Wrapper<JujiageliEntity> wrapper);
	

}
