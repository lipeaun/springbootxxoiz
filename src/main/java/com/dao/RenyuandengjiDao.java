package com.dao;

import com.model.RenyuandengjiEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.RenyuandengjiView;


/**
 * 人员登记
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface RenyuandengjiDao extends BaseMapper<RenyuandengjiEntity> {

	List<RenyuandengjiView> selectListView(@Param("ew") Wrapper<RenyuandengjiEntity> wrapper);

	List<RenyuandengjiView> selectListView(Pagination page,@Param("ew") Wrapper<RenyuandengjiEntity> wrapper);
	
	RenyuandengjiView selectView(@Param("ew") Wrapper<RenyuandengjiEntity> wrapper);
	

}
