package com.dao;

import com.model.HesuanjianceEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;

import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.HesuanjianceView;


/**
 * 核酸检测
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface HesuanjianceDao extends BaseMapper<HesuanjianceEntity> {

	List<HesuanjianceView> selectListView(@Param("ew") Wrapper<HesuanjianceEntity> wrapper);

	List<HesuanjianceView> selectListView(Pagination page,@Param("ew") Wrapper<HesuanjianceEntity> wrapper);
	
	HesuanjianceView selectView(@Param("ew") Wrapper<HesuanjianceEntity> wrapper);
	

}
