package com.dao;

import com.model.FengxiandiquEntity;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.plugins.pagination.Pagination;

import org.apache.ibatis.annotations.Param;
import com.model.web.FengxiandiquView;


/**
 * 风险地区
 * 
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
public interface FengxiandiquDao extends BaseMapper<FengxiandiquEntity> {

	List<FengxiandiquView> selectListView(@Param("ew") Wrapper<FengxiandiquEntity> wrapper);

	List<FengxiandiquView> selectListView(Pagination page,@Param("ew") Wrapper<FengxiandiquEntity> wrapper);
	
	FengxiandiquView selectView(@Param("ew") Wrapper<FengxiandiquEntity> wrapper);
	

    List<Map<String, Object>> selectValue(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<FengxiandiquEntity> wrapper);

    List<Map<String, Object>> selectTimeStatValue(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<FengxiandiquEntity> wrapper);

    List<Map<String, Object>> selectGroup(@Param("params") Map<String, Object> params,@Param("ew") Wrapper<FengxiandiquEntity> wrapper);
}
