package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.HashMap;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.FengxiandiquEntity;
import com.model.web.FengxiandiquView;

import com.service.FengxiandiquService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;
import com.utils.CommonUtil;
import java.io.IOException;
import java.io.InputStream;
import org.springframework.web.multipart.MultipartFile;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * 风险地区
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/fengxiandiqu")
public class FengxiandiquController {
    @Autowired
    private FengxiandiquService fengxiandiquService;


    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,FengxiandiquEntity fengxiandiqu,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("fanxiangguanliyuan")) {
			fengxiandiqu.setGuanliyuanzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<FengxiandiquEntity> ew = new EntityWrapper<FengxiandiquEntity>();
		PageUtils page = fengxiandiquService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, fengxiandiqu), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,FengxiandiquEntity fengxiandiqu, 
		HttpServletRequest request){
        EntityWrapper<FengxiandiquEntity> ew = new EntityWrapper<FengxiandiquEntity>();
		PageUtils page = fengxiandiquService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, fengxiandiqu), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( FengxiandiquEntity fengxiandiqu){
       	EntityWrapper<FengxiandiquEntity> ew = new EntityWrapper<FengxiandiquEntity>();
      	ew.allEq(MPUtil.allEQMapPre( fengxiandiqu, "fengxiandiqu")); 
        return R.ok().put("data", fengxiandiquService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(FengxiandiquEntity fengxiandiqu){
        EntityWrapper< FengxiandiquEntity> ew = new EntityWrapper< FengxiandiquEntity>();
 		ew.allEq(MPUtil.allEQMapPre( fengxiandiqu, "fengxiandiqu")); 
		FengxiandiquView fengxiandiquView =  fengxiandiquService.selectView(ew);
		return R.ok("查询风险地区成功").put("data", fengxiandiquView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        FengxiandiquEntity fengxiandiqu = fengxiandiquService.selectById(id);
        return R.ok().put("data", fengxiandiqu);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        FengxiandiquEntity fengxiandiqu = fengxiandiquService.selectById(id);
        return R.ok().put("data", fengxiandiqu);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody FengxiandiquEntity fengxiandiqu, HttpServletRequest request){
    	fengxiandiqu.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(fengxiandiqu);
        fengxiandiquService.insert(fengxiandiqu);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody FengxiandiquEntity fengxiandiqu, HttpServletRequest request){
    	fengxiandiqu.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(fengxiandiqu);
        fengxiandiquService.insert(fengxiandiqu);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody FengxiandiquEntity fengxiandiqu, HttpServletRequest request){
        //ValidatorUtils.validateEntity(fengxiandiqu);
        fengxiandiquService.updateById(fengxiandiqu);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        fengxiandiquService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<FengxiandiquEntity> wrapper = new EntityWrapper<FengxiandiquEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("fanxiangguanliyuan")) {
			wrapper.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
		}

		int count = fengxiandiquService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	





    @RequestMapping("/importExcel")
    public R importExcel(@RequestParam("file") MultipartFile file){
        try {
            //获取输入流
            InputStream inputStream = file.getInputStream();
            //创建读取工作簿
            Workbook workbook = WorkbookFactory.create(inputStream);
            //获取工作表
            Sheet sheet = workbook.getSheetAt(0);
            //获取总行
            int rows=sheet.getPhysicalNumberOfRows();
            if(rows>1){
                //获取单元格
                for (int i = 1; i < rows; i++) {
                    Row row = sheet.getRow(i);
                    FengxiandiquEntity fengxiandiquEntity =new FengxiandiquEntity();
                    fengxiandiquEntity.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
                    String diquleixing = CommonUtil.getCellValue(row.getCell(0));
                    fengxiandiquEntity.setDiquleixing(diquleixing);
                    String xianyouquezhen = CommonUtil.getCellValue(row.getCell(1));
                    fengxiandiquEntity.setXianyouquezhen(xianyouquezhen);
                    String leijiquezhen = CommonUtil.getCellValue(row.getCell(2));
                    fengxiandiquEntity.setLeijiquezhen(leijiquezhen);
                    String siwangrenshu = CommonUtil.getCellValue(row.getCell(3));
                    fengxiandiquEntity.setSiwangrenshu(siwangrenshu);
                    String zhiyurenshu = CommonUtil.getCellValue(row.getCell(4));
                    fengxiandiquEntity.setZhiyurenshu(zhiyurenshu);
                    String beizhu = CommonUtil.getCellValue(row.getCell(5));
                    fengxiandiquEntity.setBeizhu(beizhu);
                    String guanliyuanzhanghao = CommonUtil.getCellValue(row.getCell(6));
                    fengxiandiquEntity.setGuanliyuanzhanghao(guanliyuanzhanghao);
                    String guanliyuanxingming = CommonUtil.getCellValue(row.getCell(7));
                    fengxiandiquEntity.setGuanliyuanxingming(guanliyuanxingming);
                     
                    //想数据库中添加新对象
                    fengxiandiquService.insert(fengxiandiquEntity);//方法
                }
            }
            inputStream.close();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.ok("导入成功");
    }

    /**
     * （按值统计）
     */
    @RequestMapping("/value/{xColumnName}/{yColumnName}")
    public R value(@PathVariable("yColumnName") String yColumnName, @PathVariable("xColumnName") String xColumnName,HttpServletRequest request) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("xColumn", xColumnName);
        params.put("yColumn", yColumnName);
        EntityWrapper<FengxiandiquEntity> ew = new EntityWrapper<FengxiandiquEntity>();
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("fanxiangguanliyuan")) {
            ew.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
		}
        List<Map<String, Object>> result = fengxiandiquService.selectValue(params, ew);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for(Map<String, Object> m : result) {
            for(String k : m.keySet()) {
                if(m.get(k) instanceof Date) {
                    m.put(k, sdf.format((Date)m.get(k)));
                }
            }
        }
        return R.ok().put("data", result);
    }

    /**
     * （按值统计）时间统计类型
     */
    @RequestMapping("/value/{xColumnName}/{yColumnName}/{timeStatType}")
    public R valueDay(@PathVariable("yColumnName") String yColumnName, @PathVariable("xColumnName") String xColumnName, @PathVariable("timeStatType") String timeStatType,HttpServletRequest request) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("xColumn", xColumnName);
        params.put("yColumn", yColumnName);
        params.put("timeStatType", timeStatType);
        EntityWrapper<FengxiandiquEntity> ew = new EntityWrapper<FengxiandiquEntity>();
        String tableName = request.getSession().getAttribute("tableName").toString();
        if(tableName.equals("fanxiangguanliyuan")) {
            ew.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
        }
        List<Map<String, Object>> result = fengxiandiquService.selectTimeStatValue(params, ew);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for(Map<String, Object> m : result) {
            for(String k : m.keySet()) {
                if(m.get(k) instanceof Date) {
                    m.put(k, sdf.format((Date)m.get(k)));
                }
            }
        }
        return R.ok().put("data", result);
    }

    /**
     * 分组统计
     */
    @RequestMapping("/group/{columnName}")
    public R group(@PathVariable("columnName") String columnName,HttpServletRequest request) {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("column", columnName);
        EntityWrapper<FengxiandiquEntity> ew = new EntityWrapper<FengxiandiquEntity>();
        String tableName = request.getSession().getAttribute("tableName").toString();
        if(tableName.equals("fanxiangguanliyuan")) {
            ew.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
        }
        List<Map<String, Object>> result = fengxiandiquService.selectGroup(params, ew);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        for(Map<String, Object> m : result) {
            for(String k : m.keySet()) {
                if(m.get(k) instanceof Date) {
                    m.put(k, sdf.format((Date)m.get(k)));
                }
            }
        }
        return R.ok().put("data", result);
    }

}
