package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.FangyiwuziEntity;
import com.model.web.FangyiwuziView;

import com.service.FangyiwuziService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 防疫物资
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/fangyiwuzi")
public class FangyiwuziController {
    @Autowired
    private FangyiwuziService fangyiwuziService;


    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,FangyiwuziEntity fangyiwuzi,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("wuziguanliyuan")) {
			fangyiwuzi.setGuanliyuanzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<FangyiwuziEntity> ew = new EntityWrapper<FangyiwuziEntity>();
		PageUtils page = fangyiwuziService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, fangyiwuzi), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,FangyiwuziEntity fangyiwuzi, 
		HttpServletRequest request){
        EntityWrapper<FangyiwuziEntity> ew = new EntityWrapper<FangyiwuziEntity>();
		PageUtils page = fangyiwuziService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, fangyiwuzi), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( FangyiwuziEntity fangyiwuzi){
       	EntityWrapper<FangyiwuziEntity> ew = new EntityWrapper<FangyiwuziEntity>();
      	ew.allEq(MPUtil.allEQMapPre( fangyiwuzi, "fangyiwuzi")); 
        return R.ok().put("data", fangyiwuziService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(FangyiwuziEntity fangyiwuzi){
        EntityWrapper< FangyiwuziEntity> ew = new EntityWrapper< FangyiwuziEntity>();
 		ew.allEq(MPUtil.allEQMapPre( fangyiwuzi, "fangyiwuzi")); 
		FangyiwuziView fangyiwuziView =  fangyiwuziService.selectView(ew);
		return R.ok("查询防疫物资成功").put("data", fangyiwuziView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        FangyiwuziEntity fangyiwuzi = fangyiwuziService.selectById(id);
        return R.ok().put("data", fangyiwuzi);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        FangyiwuziEntity fangyiwuzi = fangyiwuziService.selectById(id);
        return R.ok().put("data", fangyiwuzi);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody FangyiwuziEntity fangyiwuzi, HttpServletRequest request){
    	fangyiwuzi.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(fangyiwuzi);
        fangyiwuziService.insert(fangyiwuzi);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody FangyiwuziEntity fangyiwuzi, HttpServletRequest request){
    	fangyiwuzi.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(fangyiwuzi);
        fangyiwuziService.insert(fangyiwuzi);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody FangyiwuziEntity fangyiwuzi, HttpServletRequest request){
        //ValidatorUtils.validateEntity(fangyiwuzi);
        fangyiwuziService.updateById(fangyiwuzi);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        fangyiwuziService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<FangyiwuziEntity> wrapper = new EntityWrapper<FangyiwuziEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("wuziguanliyuan")) {
			wrapper.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
		}

		int count = fangyiwuziService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
