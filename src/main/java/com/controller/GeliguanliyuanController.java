package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.GeliguanliyuanEntity;
import com.model.web.GeliguanliyuanView;

import com.service.GeliguanliyuanService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 隔离管理员
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/geliguanliyuan")
public class GeliguanliyuanController {
    @Autowired
    private GeliguanliyuanService geliguanliyuanService;


    
	@Autowired
	private TokenService tokenService;
	
	/**
	 * 登录
	 */
	@IgnoreAuth
	@RequestMapping(value = "/login")
	public R login(String username, String password, String captcha, HttpServletRequest request) {
		GeliguanliyuanEntity user = geliguanliyuanService.selectOne(new EntityWrapper<GeliguanliyuanEntity>().eq("guanliyuanzhanghao", username));
		if(user==null || !user.getMima().equals(password)) {
			return R.error("账号或密码不正确");
		}
		
		String token = tokenService.generateToken(user.getId(), username,"geliguanliyuan",  "隔离管理员" );
		return R.ok().put("token", token);
	}
	
	/**
     * 注册
     */
	@IgnoreAuth
    @RequestMapping("/register")
    public R register(@RequestBody GeliguanliyuanEntity geliguanliyuan){
    	//ValidatorUtils.validateEntity(geliguanliyuan);
    	GeliguanliyuanEntity user = geliguanliyuanService.selectOne(new EntityWrapper<GeliguanliyuanEntity>().eq("guanliyuanzhanghao", geliguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("注册用户已存在");
		}
		Long uId = new Date().getTime();
		geliguanliyuan.setId(uId);
        geliguanliyuanService.insert(geliguanliyuan);
        return R.ok();
    }

	
	/**
	 * 退出
	 */
	@RequestMapping("/logout")
	public R logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return R.ok("退出成功");
	}
	
	/**
     * 获取用户的session用户信息
     */
    @RequestMapping("/session")
    public R getCurrUser(HttpServletRequest request){
    	Long id = (Long)request.getSession().getAttribute("userId");
        GeliguanliyuanEntity user = geliguanliyuanService.selectById(id);
        return R.ok().put("data", user);
    }
    
    /**
     * 密码重置
     */
    @IgnoreAuth
	@RequestMapping(value = "/resetPass")
    public R resetPass(String username, HttpServletRequest request){
    	GeliguanliyuanEntity user = geliguanliyuanService.selectOne(new EntityWrapper<GeliguanliyuanEntity>().eq("guanliyuanzhanghao", username));
    	if(user==null) {
    		return R.error("账号不存在");
    	}
        user.setMima("123456");
        geliguanliyuanService.updateById(user);
        return R.ok("密码已重置为：123456");
    }


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,GeliguanliyuanEntity geliguanliyuan,
		HttpServletRequest request){
        EntityWrapper<GeliguanliyuanEntity> ew = new EntityWrapper<GeliguanliyuanEntity>();
		PageUtils page = geliguanliyuanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, geliguanliyuan), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,GeliguanliyuanEntity geliguanliyuan, 
		HttpServletRequest request){
        EntityWrapper<GeliguanliyuanEntity> ew = new EntityWrapper<GeliguanliyuanEntity>();
		PageUtils page = geliguanliyuanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, geliguanliyuan), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( GeliguanliyuanEntity geliguanliyuan){
       	EntityWrapper<GeliguanliyuanEntity> ew = new EntityWrapper<GeliguanliyuanEntity>();
      	ew.allEq(MPUtil.allEQMapPre( geliguanliyuan, "geliguanliyuan")); 
        return R.ok().put("data", geliguanliyuanService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(GeliguanliyuanEntity geliguanliyuan){
        EntityWrapper< GeliguanliyuanEntity> ew = new EntityWrapper< GeliguanliyuanEntity>();
 		ew.allEq(MPUtil.allEQMapPre( geliguanliyuan, "geliguanliyuan")); 
		GeliguanliyuanView geliguanliyuanView =  geliguanliyuanService.selectView(ew);
		return R.ok("查询隔离管理员成功").put("data", geliguanliyuanView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        GeliguanliyuanEntity geliguanliyuan = geliguanliyuanService.selectById(id);
        return R.ok().put("data", geliguanliyuan);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        GeliguanliyuanEntity geliguanliyuan = geliguanliyuanService.selectById(id);
        return R.ok().put("data", geliguanliyuan);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody GeliguanliyuanEntity geliguanliyuan, HttpServletRequest request){
    	geliguanliyuan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(geliguanliyuan);
    	GeliguanliyuanEntity user = geliguanliyuanService.selectOne(new EntityWrapper<GeliguanliyuanEntity>().eq("guanliyuanzhanghao", geliguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("用户已存在");
		}
		geliguanliyuan.setId(new Date().getTime());
        geliguanliyuanService.insert(geliguanliyuan);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody GeliguanliyuanEntity geliguanliyuan, HttpServletRequest request){
    	geliguanliyuan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(geliguanliyuan);
    	GeliguanliyuanEntity user = geliguanliyuanService.selectOne(new EntityWrapper<GeliguanliyuanEntity>().eq("guanliyuanzhanghao", geliguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("用户已存在");
		}
		geliguanliyuan.setId(new Date().getTime());
        geliguanliyuanService.insert(geliguanliyuan);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody GeliguanliyuanEntity geliguanliyuan, HttpServletRequest request){
        //ValidatorUtils.validateEntity(geliguanliyuan);
        geliguanliyuanService.updateById(geliguanliyuan);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        geliguanliyuanService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<GeliguanliyuanEntity> wrapper = new EntityWrapper<GeliguanliyuanEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = geliguanliyuanService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
