package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.WuziguanliyuanEntity;
import com.model.web.WuziguanliyuanView;

import com.service.WuziguanliyuanService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 物资管理员
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/wuziguanliyuan")
public class WuziguanliyuanController {
    @Autowired
    private WuziguanliyuanService wuziguanliyuanService;


    
	@Autowired
	private TokenService tokenService;
	
	/**
	 * 登录
	 */
	@IgnoreAuth
	@RequestMapping(value = "/login")
	public R login(String username, String password, String captcha, HttpServletRequest request) {
		WuziguanliyuanEntity user = wuziguanliyuanService.selectOne(new EntityWrapper<WuziguanliyuanEntity>().eq("guanliyuanzhanghao", username));
		if(user==null || !user.getMima().equals(password)) {
			return R.error("账号或密码不正确");
		}
		
		String token = tokenService.generateToken(user.getId(), username,"wuziguanliyuan",  "物资管理员" );
		return R.ok().put("token", token);
	}
	
	/**
     * 注册
     */
	@IgnoreAuth
    @RequestMapping("/register")
    public R register(@RequestBody WuziguanliyuanEntity wuziguanliyuan){
    	//ValidatorUtils.validateEntity(wuziguanliyuan);
    	WuziguanliyuanEntity user = wuziguanliyuanService.selectOne(new EntityWrapper<WuziguanliyuanEntity>().eq("guanliyuanzhanghao", wuziguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("注册用户已存在");
		}
		Long uId = new Date().getTime();
		wuziguanliyuan.setId(uId);
        wuziguanliyuanService.insert(wuziguanliyuan);
        return R.ok();
    }

	
	/**
	 * 退出
	 */
	@RequestMapping("/logout")
	public R logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return R.ok("退出成功");
	}
	
	/**
     * 获取用户的session用户信息
     */
    @RequestMapping("/session")
    public R getCurrUser(HttpServletRequest request){
    	Long id = (Long)request.getSession().getAttribute("userId");
        WuziguanliyuanEntity user = wuziguanliyuanService.selectById(id);
        return R.ok().put("data", user);
    }
    
    /**
     * 密码重置
     */
    @IgnoreAuth
	@RequestMapping(value = "/resetPass")
    public R resetPass(String username, HttpServletRequest request){
    	WuziguanliyuanEntity user = wuziguanliyuanService.selectOne(new EntityWrapper<WuziguanliyuanEntity>().eq("guanliyuanzhanghao", username));
    	if(user==null) {
    		return R.error("账号不存在");
    	}
        user.setMima("123456");
        wuziguanliyuanService.updateById(user);
        return R.ok("密码已重置为：123456");
    }


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,WuziguanliyuanEntity wuziguanliyuan,
		HttpServletRequest request){
        EntityWrapper<WuziguanliyuanEntity> ew = new EntityWrapper<WuziguanliyuanEntity>();
		PageUtils page = wuziguanliyuanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, wuziguanliyuan), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,WuziguanliyuanEntity wuziguanliyuan, 
		HttpServletRequest request){
        EntityWrapper<WuziguanliyuanEntity> ew = new EntityWrapper<WuziguanliyuanEntity>();
		PageUtils page = wuziguanliyuanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, wuziguanliyuan), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( WuziguanliyuanEntity wuziguanliyuan){
       	EntityWrapper<WuziguanliyuanEntity> ew = new EntityWrapper<WuziguanliyuanEntity>();
      	ew.allEq(MPUtil.allEQMapPre( wuziguanliyuan, "wuziguanliyuan")); 
        return R.ok().put("data", wuziguanliyuanService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(WuziguanliyuanEntity wuziguanliyuan){
        EntityWrapper< WuziguanliyuanEntity> ew = new EntityWrapper< WuziguanliyuanEntity>();
 		ew.allEq(MPUtil.allEQMapPre( wuziguanliyuan, "wuziguanliyuan")); 
		WuziguanliyuanView wuziguanliyuanView =  wuziguanliyuanService.selectView(ew);
		return R.ok("查询物资管理员成功").put("data", wuziguanliyuanView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        WuziguanliyuanEntity wuziguanliyuan = wuziguanliyuanService.selectById(id);
        return R.ok().put("data", wuziguanliyuan);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        WuziguanliyuanEntity wuziguanliyuan = wuziguanliyuanService.selectById(id);
        return R.ok().put("data", wuziguanliyuan);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody WuziguanliyuanEntity wuziguanliyuan, HttpServletRequest request){
    	wuziguanliyuan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(wuziguanliyuan);
    	WuziguanliyuanEntity user = wuziguanliyuanService.selectOne(new EntityWrapper<WuziguanliyuanEntity>().eq("guanliyuanzhanghao", wuziguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("用户已存在");
		}
		wuziguanliyuan.setId(new Date().getTime());
        wuziguanliyuanService.insert(wuziguanliyuan);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody WuziguanliyuanEntity wuziguanliyuan, HttpServletRequest request){
    	wuziguanliyuan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(wuziguanliyuan);
    	WuziguanliyuanEntity user = wuziguanliyuanService.selectOne(new EntityWrapper<WuziguanliyuanEntity>().eq("guanliyuanzhanghao", wuziguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("用户已存在");
		}
		wuziguanliyuan.setId(new Date().getTime());
        wuziguanliyuanService.insert(wuziguanliyuan);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody WuziguanliyuanEntity wuziguanliyuan, HttpServletRequest request){
        //ValidatorUtils.validateEntity(wuziguanliyuan);
        wuziguanliyuanService.updateById(wuziguanliyuan);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        wuziguanliyuanService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<WuziguanliyuanEntity> wrapper = new EntityWrapper<WuziguanliyuanEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = wuziguanliyuanService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
