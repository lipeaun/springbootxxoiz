package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.RizhijiluEntity;
import com.model.web.RizhijiluView;

import com.service.RizhijiluService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 日志记录
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/rizhijilu")
public class RizhijiluController {
    @Autowired
    private RizhijiluService rizhijiluService;


    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,RizhijiluEntity rizhijilu,
		HttpServletRequest request){
        EntityWrapper<RizhijiluEntity> ew = new EntityWrapper<RizhijiluEntity>();
		PageUtils page = rizhijiluService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, rizhijilu), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,RizhijiluEntity rizhijilu, 
		HttpServletRequest request){
        EntityWrapper<RizhijiluEntity> ew = new EntityWrapper<RizhijiluEntity>();
		PageUtils page = rizhijiluService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, rizhijilu), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( RizhijiluEntity rizhijilu){
       	EntityWrapper<RizhijiluEntity> ew = new EntityWrapper<RizhijiluEntity>();
      	ew.allEq(MPUtil.allEQMapPre( rizhijilu, "rizhijilu")); 
        return R.ok().put("data", rizhijiluService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(RizhijiluEntity rizhijilu){
        EntityWrapper< RizhijiluEntity> ew = new EntityWrapper< RizhijiluEntity>();
 		ew.allEq(MPUtil.allEQMapPre( rizhijilu, "rizhijilu")); 
		RizhijiluView rizhijiluView =  rizhijiluService.selectView(ew);
		return R.ok("查询日志记录成功").put("data", rizhijiluView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        RizhijiluEntity rizhijilu = rizhijiluService.selectById(id);
        return R.ok().put("data", rizhijilu);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        RizhijiluEntity rizhijilu = rizhijiluService.selectById(id);
        return R.ok().put("data", rizhijilu);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody RizhijiluEntity rizhijilu, HttpServletRequest request){
    	rizhijilu.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(rizhijilu);
        rizhijiluService.insert(rizhijilu);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody RizhijiluEntity rizhijilu, HttpServletRequest request){
    	rizhijilu.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(rizhijilu);
        rizhijiluService.insert(rizhijilu);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody RizhijiluEntity rizhijilu, HttpServletRequest request){
        //ValidatorUtils.validateEntity(rizhijilu);
        rizhijiluService.updateById(rizhijilu);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        rizhijiluService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<RizhijiluEntity> wrapper = new EntityWrapper<RizhijiluEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = rizhijiluService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
