package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.JujiageliEntity;
import com.model.web.JujiageliView;

import com.service.JujiageliService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 居家隔离
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/jujiageli")
public class JujiageliController {
    @Autowired
    private JujiageliService jujiageliService;


    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,JujiageliEntity jujiageli,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			jujiageli.setYonghuzhanghao((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("geliguanliyuan")) {
			jujiageli.setGuanliyuanzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<JujiageliEntity> ew = new EntityWrapper<JujiageliEntity>();
		PageUtils page = jujiageliService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, jujiageli), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,JujiageliEntity jujiageli, 
		HttpServletRequest request){
        EntityWrapper<JujiageliEntity> ew = new EntityWrapper<JujiageliEntity>();
		PageUtils page = jujiageliService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, jujiageli), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( JujiageliEntity jujiageli){
       	EntityWrapper<JujiageliEntity> ew = new EntityWrapper<JujiageliEntity>();
      	ew.allEq(MPUtil.allEQMapPre( jujiageli, "jujiageli")); 
        return R.ok().put("data", jujiageliService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(JujiageliEntity jujiageli){
        EntityWrapper< JujiageliEntity> ew = new EntityWrapper< JujiageliEntity>();
 		ew.allEq(MPUtil.allEQMapPre( jujiageli, "jujiageli")); 
		JujiageliView jujiageliView =  jujiageliService.selectView(ew);
		return R.ok("查询居家隔离成功").put("data", jujiageliView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        JujiageliEntity jujiageli = jujiageliService.selectById(id);
        return R.ok().put("data", jujiageli);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        JujiageliEntity jujiageli = jujiageliService.selectById(id);
        return R.ok().put("data", jujiageli);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody JujiageliEntity jujiageli, HttpServletRequest request){
    	jujiageli.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(jujiageli);
        jujiageliService.insert(jujiageli);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody JujiageliEntity jujiageli, HttpServletRequest request){
    	jujiageli.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(jujiageli);
        jujiageliService.insert(jujiageli);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody JujiageliEntity jujiageli, HttpServletRequest request){
        //ValidatorUtils.validateEntity(jujiageli);
        jujiageliService.updateById(jujiageli);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        jujiageliService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<JujiageliEntity> wrapper = new EntityWrapper<JujiageliEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			wrapper.eq("yonghuzhanghao", (String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("geliguanliyuan")) {
			wrapper.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
		}

		int count = jujiageliService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
