package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.RenyuandengjiEntity;
import com.model.web.RenyuandengjiView;

import com.service.RenyuandengjiService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 人员登记
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/renyuandengji")
public class RenyuandengjiController {
    @Autowired
    private RenyuandengjiService renyuandengjiService;


    


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,RenyuandengjiEntity renyuandengji,
		HttpServletRequest request){
		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			renyuandengji.setYonghuzhanghao((String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("fanxiangguanliyuan")) {
			renyuandengji.setGuanliyuanzhanghao((String)request.getSession().getAttribute("username"));
		}
        EntityWrapper<RenyuandengjiEntity> ew = new EntityWrapper<RenyuandengjiEntity>();
		PageUtils page = renyuandengjiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, renyuandengji), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,RenyuandengjiEntity renyuandengji, 
		HttpServletRequest request){
        EntityWrapper<RenyuandengjiEntity> ew = new EntityWrapper<RenyuandengjiEntity>();
		PageUtils page = renyuandengjiService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, renyuandengji), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( RenyuandengjiEntity renyuandengji){
       	EntityWrapper<RenyuandengjiEntity> ew = new EntityWrapper<RenyuandengjiEntity>();
      	ew.allEq(MPUtil.allEQMapPre( renyuandengji, "renyuandengji")); 
        return R.ok().put("data", renyuandengjiService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(RenyuandengjiEntity renyuandengji){
        EntityWrapper< RenyuandengjiEntity> ew = new EntityWrapper< RenyuandengjiEntity>();
 		ew.allEq(MPUtil.allEQMapPre( renyuandengji, "renyuandengji")); 
		RenyuandengjiView renyuandengjiView =  renyuandengjiService.selectView(ew);
		return R.ok("查询人员登记成功").put("data", renyuandengjiView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        RenyuandengjiEntity renyuandengji = renyuandengjiService.selectById(id);
        return R.ok().put("data", renyuandengji);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        RenyuandengjiEntity renyuandengji = renyuandengjiService.selectById(id);
        return R.ok().put("data", renyuandengji);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody RenyuandengjiEntity renyuandengji, HttpServletRequest request){
    	renyuandengji.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(renyuandengji);
        renyuandengjiService.insert(renyuandengji);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody RenyuandengjiEntity renyuandengji, HttpServletRequest request){
    	renyuandengji.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(renyuandengji);
        renyuandengjiService.insert(renyuandengji);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody RenyuandengjiEntity renyuandengji, HttpServletRequest request){
        //ValidatorUtils.validateEntity(renyuandengji);
        renyuandengjiService.updateById(renyuandengji);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        renyuandengjiService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<RenyuandengjiEntity> wrapper = new EntityWrapper<RenyuandengjiEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}

		String tableName = request.getSession().getAttribute("tableName").toString();
		if(tableName.equals("yonghu")) {
			wrapper.eq("yonghuzhanghao", (String)request.getSession().getAttribute("username"));
		}
		if(tableName.equals("fanxiangguanliyuan")) {
			wrapper.eq("guanliyuanzhanghao", (String)request.getSession().getAttribute("username"));
		}

		int count = renyuandengjiService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
