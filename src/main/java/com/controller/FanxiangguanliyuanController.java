package com.controller;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Map;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.annotation.IgnoreAuth;

import com.model.FanxiangguanliyuanEntity;
import com.model.web.FanxiangguanliyuanView;

import com.service.FanxiangguanliyuanService;
import com.service.TokenService;
import com.utils.PageUtils;
import com.utils.R;
import com.utils.MPUtil;

/**
 * 返乡管理员
 * 后端接口
 * @author 
 * @email 
 * @date 2022-02-11 20:41:38
 */
@RestController
@RequestMapping("/fanxiangguanliyuan")
public class FanxiangguanliyuanController {
    @Autowired
    private FanxiangguanliyuanService fanxiangguanliyuanService;


    
	@Autowired
	private TokenService tokenService;
	
	/**
	 * 登录
	 */
	@IgnoreAuth
	@RequestMapping(value = "/login")
	public R login(String username, String password, String captcha, HttpServletRequest request) {
		FanxiangguanliyuanEntity user = fanxiangguanliyuanService.selectOne(new EntityWrapper<FanxiangguanliyuanEntity>().eq("guanliyuanzhanghao", username));
		if(user==null || !user.getMima().equals(password)) {
			return R.error("账号或密码不正确");
		}
		
		String token = tokenService.generateToken(user.getId(), username,"fanxiangguanliyuan",  "返乡管理员" );
		return R.ok().put("token", token);
	}
	
	/**
     * 注册
     */
	@IgnoreAuth
    @RequestMapping("/register")
    public R register(@RequestBody FanxiangguanliyuanEntity fanxiangguanliyuan){
    	//ValidatorUtils.validateEntity(fanxiangguanliyuan);
    	FanxiangguanliyuanEntity user = fanxiangguanliyuanService.selectOne(new EntityWrapper<FanxiangguanliyuanEntity>().eq("guanliyuanzhanghao", fanxiangguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("注册用户已存在");
		}
		Long uId = new Date().getTime();
		fanxiangguanliyuan.setId(uId);
        fanxiangguanliyuanService.insert(fanxiangguanliyuan);
        return R.ok();
    }

	
	/**
	 * 退出
	 */
	@RequestMapping("/logout")
	public R logout(HttpServletRequest request) {
		request.getSession().invalidate();
		return R.ok("退出成功");
	}
	
	/**
     * 获取用户的session用户信息
     */
    @RequestMapping("/session")
    public R getCurrUser(HttpServletRequest request){
    	Long id = (Long)request.getSession().getAttribute("userId");
        FanxiangguanliyuanEntity user = fanxiangguanliyuanService.selectById(id);
        return R.ok().put("data", user);
    }
    
    /**
     * 密码重置
     */
    @IgnoreAuth
	@RequestMapping(value = "/resetPass")
    public R resetPass(String username, HttpServletRequest request){
    	FanxiangguanliyuanEntity user = fanxiangguanliyuanService.selectOne(new EntityWrapper<FanxiangguanliyuanEntity>().eq("guanliyuanzhanghao", username));
    	if(user==null) {
    		return R.error("账号不存在");
    	}
        user.setMima("123456");
        fanxiangguanliyuanService.updateById(user);
        return R.ok("密码已重置为：123456");
    }


    /**
     * 后端列表
     */
    @RequestMapping("/page")
    public R page(@RequestParam Map<String, Object> params,FanxiangguanliyuanEntity fanxiangguanliyuan,
		HttpServletRequest request){
        EntityWrapper<FanxiangguanliyuanEntity> ew = new EntityWrapper<FanxiangguanliyuanEntity>();
		PageUtils page = fanxiangguanliyuanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, fanxiangguanliyuan), params), params));

        return R.ok().put("data", page);
    }
    
    /**
     * 前端列表
     */
	@IgnoreAuth
    @RequestMapping("/list")
    public R list(@RequestParam Map<String, Object> params,FanxiangguanliyuanEntity fanxiangguanliyuan, 
		HttpServletRequest request){
        EntityWrapper<FanxiangguanliyuanEntity> ew = new EntityWrapper<FanxiangguanliyuanEntity>();
		PageUtils page = fanxiangguanliyuanService.queryPage(params, MPUtil.sort(MPUtil.between(MPUtil.likeOrEq(ew, fanxiangguanliyuan), params), params));
        return R.ok().put("data", page);
    }

	/**
     * 列表
     */
    @RequestMapping("/lists")
    public R list( FanxiangguanliyuanEntity fanxiangguanliyuan){
       	EntityWrapper<FanxiangguanliyuanEntity> ew = new EntityWrapper<FanxiangguanliyuanEntity>();
      	ew.allEq(MPUtil.allEQMapPre( fanxiangguanliyuan, "fanxiangguanliyuan")); 
        return R.ok().put("data", fanxiangguanliyuanService.selectListView(ew));
    }

	 /**
     * 查询
     */
    @RequestMapping("/query")
    public R query(FanxiangguanliyuanEntity fanxiangguanliyuan){
        EntityWrapper< FanxiangguanliyuanEntity> ew = new EntityWrapper< FanxiangguanliyuanEntity>();
 		ew.allEq(MPUtil.allEQMapPre( fanxiangguanliyuan, "fanxiangguanliyuan")); 
		FanxiangguanliyuanView fanxiangguanliyuanView =  fanxiangguanliyuanService.selectView(ew);
		return R.ok("查询返乡管理员成功").put("data", fanxiangguanliyuanView);
    }
	
    /**
     * 后端详情
     */
    @RequestMapping("/info/{id}")
    public R info(@PathVariable("id") Long id){
        FanxiangguanliyuanEntity fanxiangguanliyuan = fanxiangguanliyuanService.selectById(id);
        return R.ok().put("data", fanxiangguanliyuan);
    }

    /**
     * 前端详情
     */
	@IgnoreAuth
    @RequestMapping("/detail/{id}")
    public R detail(@PathVariable("id") Long id){
        FanxiangguanliyuanEntity fanxiangguanliyuan = fanxiangguanliyuanService.selectById(id);
        return R.ok().put("data", fanxiangguanliyuan);
    }
    



    /**
     * 后端保存
     */
    @RequestMapping("/save")
    public R save(@RequestBody FanxiangguanliyuanEntity fanxiangguanliyuan, HttpServletRequest request){
    	fanxiangguanliyuan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(fanxiangguanliyuan);
    	FanxiangguanliyuanEntity user = fanxiangguanliyuanService.selectOne(new EntityWrapper<FanxiangguanliyuanEntity>().eq("guanliyuanzhanghao", fanxiangguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("用户已存在");
		}
		fanxiangguanliyuan.setId(new Date().getTime());
        fanxiangguanliyuanService.insert(fanxiangguanliyuan);
        return R.ok();
    }
    
    /**
     * 前端保存
     */
    @RequestMapping("/add")
    public R add(@RequestBody FanxiangguanliyuanEntity fanxiangguanliyuan, HttpServletRequest request){
    	fanxiangguanliyuan.setId(new Date().getTime()+new Double(Math.floor(Math.random()*1000)).longValue());
    	//ValidatorUtils.validateEntity(fanxiangguanliyuan);
    	FanxiangguanliyuanEntity user = fanxiangguanliyuanService.selectOne(new EntityWrapper<FanxiangguanliyuanEntity>().eq("guanliyuanzhanghao", fanxiangguanliyuan.getGuanliyuanzhanghao()));
		if(user!=null) {
			return R.error("用户已存在");
		}
		fanxiangguanliyuan.setId(new Date().getTime());
        fanxiangguanliyuanService.insert(fanxiangguanliyuan);
        return R.ok();
    }

    /**
     * 修改
     */
    @RequestMapping("/update")
    public R update(@RequestBody FanxiangguanliyuanEntity fanxiangguanliyuan, HttpServletRequest request){
        //ValidatorUtils.validateEntity(fanxiangguanliyuan);
        fanxiangguanliyuanService.updateById(fanxiangguanliyuan);//全部更新
        return R.ok();
    }
    

    /**
     * 删除
     */
    @RequestMapping("/delete")
    public R delete(@RequestBody Long[] ids){
        fanxiangguanliyuanService.deleteBatchIds(Arrays.asList(ids));
        return R.ok();
    }
    
    /**
     * 提醒接口
     */
	@RequestMapping("/remind/{columnName}/{type}")
	public R remindCount(@PathVariable("columnName") String columnName, HttpServletRequest request, 
						 @PathVariable("type") String type,@RequestParam Map<String, Object> map) {
		map.put("column", columnName);
		map.put("type", type);
		
		if(type.equals("2")) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Calendar c = Calendar.getInstance();
			Date remindStartDate = null;
			Date remindEndDate = null;
			if(map.get("remindstart")!=null) {
				Integer remindStart = Integer.parseInt(map.get("remindstart").toString());
				c.setTime(new Date()); 
				c.add(Calendar.DAY_OF_MONTH,remindStart);
				remindStartDate = c.getTime();
				map.put("remindstart", sdf.format(remindStartDate));
			}
			if(map.get("remindend")!=null) {
				Integer remindEnd = Integer.parseInt(map.get("remindend").toString());
				c.setTime(new Date());
				c.add(Calendar.DAY_OF_MONTH,remindEnd);
				remindEndDate = c.getTime();
				map.put("remindend", sdf.format(remindEndDate));
			}
		}
		
		Wrapper<FanxiangguanliyuanEntity> wrapper = new EntityWrapper<FanxiangguanliyuanEntity>();
		if(map.get("remindstart")!=null) {
			wrapper.ge(columnName, map.get("remindstart"));
		}
		if(map.get("remindend")!=null) {
			wrapper.le(columnName, map.get("remindend"));
		}


		int count = fanxiangguanliyuanService.selectCount(wrapper);
		return R.ok().put("count", count);
	}
	







}
